require('./pdftofoundry.scss');

import * as PDFJS from "pdfjs-dist";

import * as Foundry from './foundry'
import { parsePdf, TextDetail } from './parse'
import adventures from './modules'
import { ModuleDescription, ModuleData } from "./modules/IAdventure";

import { IModuleOutput} from './IModuleOutput';
import { ModuleImporter } from "./modules/ModuleImporter";

PDFJS.GlobalWorkerOptions.workerSrc = "./modules/pdftofoundry/pdf.worker.bundle.js";

const readUploadedFileAsBinary = (inputFile) => {
    const temporaryFileReader = new FileReader();

    return new Promise<ArrayBuffer>((resolve, reject) => {
        temporaryFileReader.onerror = () => {
            temporaryFileReader.abort();
            reject(new DOMException("Problem parsing input file."));
        };

        temporaryFileReader.onload = () => {
            resolve(temporaryFileReader.result as ArrayBuffer);
        };
        temporaryFileReader.readAsArrayBuffer(inputFile);
    });
};

const readUploadedFileAsText = (inputFile) => {
    const temporaryFileReader = new FileReader();

    return new Promise<string>((resolve, reject) => {
        temporaryFileReader.onerror = () => {
            temporaryFileReader.abort();
            reject(new DOMException("Problem parsing input file."));
        };

        temporaryFileReader.onload = () => {
            resolve(temporaryFileReader.result as string);
        };
        temporaryFileReader.readAsText(inputFile);
    });
};

/**
 * Extract the datasource from the path
 * "[s3:bucketname] path"
 * "[data] path"
 * "[core] path"
 * @param {string} val A reference to the target path coming from settingsextender (patched)
 */
const getDataSource = (val) => {
    let source = "data";
    let path = val;

    // check if we are using the patched settings extender
    let matches = val.trim().match(/\[(.+)\]\s*(.+)/);
    if (matches) {
        // we do
        source = matches[1];
        // get bucket information, if S3 is used
        const [s3Source, bucket] = source.split(":");
        if (bucket !== undefined) {
            return {
                source: s3Source,
                bucket: bucket,
                path: matches[2],
            };
        } else {
            return {
                source: source,
                bucket: null,
                path: matches[2],
            };
        }
    } else {
        return {
            source: source,
            path: path,
        };
    }
};

async function uploadToFoundryV2(data, path, filename) {
    return new Promise(async (resolve, reject) => {
        // create new file from the response
        let file = new File([data], filename, { type: data.type });

        const target = getDataSource(path);
        let result = await FilePicker.upload(
            target.source,
            target.path,
            file,
            target.bucket ? { bucket: target.bucket } : {}
        );
        resolve(target.path);
    });
}

async function mkdirFoundry(filename) {
    return new Promise(async (resolve, reject) => {
        // create new file from the response

        const target = getDataSource(filename);
        try {
            // @ts-ignore
            let result = await FilePicker.createDirectory(
                target.source,
                target.path,
                target.bucket ? { bucket: target.bucket } : {}
            );
            resolve(target.path);
        } catch (error) {
            reject(error);
        }
    });
}

function sleep(duration: number) : Promise<void> {
    return new Promise(resolve => setTimeout(resolve, duration));
}

class FoundryModuleOutput implements IModuleOutput {
    journalFolder: Entity;
    actorFolder: Entity;
    sceneFolder: Entity;
    itemFolder: Entity;
    moduleFullName: string;
    moduleBaseFolder: string;
    createdFolders: string[] = [];

    constructor(moduleFullName: string, moduleBaseFolder: string) {
        this.moduleFullName = moduleFullName;
        this.moduleBaseFolder = moduleBaseFolder;
    }

    progressFn(status: string, percentComplete: number) {
        progressFn(status, percentComplete);
    }

    async ensureFolderExists(path: string) {
        if (this.createdFolders.includes(path)) {
            return;
        }

        const sections = path.split("/");
        if (sections.length > 1) {
            await this.ensureFolderExists(sections.slice(0, sections.length-1).join("/"));
        }
        
        try {
            console.log(`creating folder '${path}'..`);
            await mkdirFoundry(path);
        } catch {
        }
        await sleep(500); // for some reason it takes a hot second for the folder to be writeable
        console.log(`..done`);

        this.createdFolders.push(path);
    }

    async ensureSceneFolder() {
        if (this.sceneFolder === undefined) {
            this.sceneFolder = await Folder.create({name: this.moduleFullName, type: 'Scene', parent: null})
        }
    }

    async ensureJournalFolder() {
        if (this.journalFolder === undefined) {
            this.journalFolder = await Folder.create({name: this.moduleFullName, type: 'JournalEntry', parent: null})
        }
    }
    
    async ensureActorFolder() {
        if (this.actorFolder === undefined) {
            this.actorFolder = await Folder.create({name: this.moduleFullName, type: 'Actor', parent: null})
        }
    }
    
    async ensureItemFolder() {
        if (this.itemFolder === undefined) {
            this.itemFolder = await Folder.create({name: this.moduleFullName, type: 'Item', parent: null})
        }
    }

    async emitFile(path: string, name: string, data: string | Blob) {
        console.log(`emitting file to [${this.moduleBaseFolder}] [${path}] [${name}]`);
        await this.ensureFolderExists(`${this.moduleBaseFolder}/${path}`);
        await uploadToFoundryV2(data, `${this.moduleBaseFolder}/${path}`, name);
        return `${this.moduleBaseFolder}/${path}/${name}`;
    }

    async createScene(scene: Foundry.Scene) {
        await this.ensureSceneFolder();
        const savedImg = scene.img;
        scene.img = '';
        scene.folder = this.sceneFolder.id;
        let ret = await Scene.create(scene);
        scene._id = ret.id;

        // thumbnail generation only seems to works when adding the img after it's been created
        // and only if the value has changed
        await ret.update({ img: savedImg });
    }

    async createJournal(journal: Foundry.Journal) {
        await this.ensureJournalFolder();
        journal.folder = this.journalFolder.id;
        let ret = await JournalEntry.create(journal);
        journal._id = ret.id;
    }

    async createActor(actor: Foundry.IActor) {
        await this.ensureActorFolder();
        actor.folder = this.actorFolder.id;
        let ret = await Actor.create(actor);
        actor._id = ret.id;
    }

    async createItem(item: Foundry.IItem) {
        await this.ensureItemFolder();
        item.folder = this.itemFolder.id;
        let ret = await Item.create(item);
        item._id = ret.id;
    }
}


function progressFn(status: string, progress: number) {
    const loader = document.getElementById("loading");
    const pct = Math.clamped(100 * progress, 0, 100);
    loader.querySelector("#context").textContent = status;
    (loader.querySelector("#loading-bar") as HTMLElement).style.width = `${pct}%`;
    loader.querySelector("#progress").textContent = '';
    loader.style.display = 'block';
    if (progress == 1 && !loader.hidden)
        $(loader).fadeOut(5000);
    //console.log(status);
}

export async function generateData(pdf: any) {
    let parsedPdf;
    if (pdf.type == 'application/json') {
        const json = await readUploadedFileAsText(pdf);
        parsedPdf = {
            textDetails: JSON.parse(json),
            imageDetails: []
        };
    } else {
        const typedArray = await readUploadedFileAsBinary(pdf);
        const buffer = new Uint8Array(typedArray);
        parsedPdf = await parsePdf(buffer, progressFn, undefined);
    }

    let adventureModule: ModuleDescription;
    adventureModule = adventures.find(a => a.detect(parsedPdf));

    if (adventureModule === undefined) {
        progressFn('failed to detect pdf. cannot proceed :(', 0);
        return;
    }
    let output = new FoundryModuleOutput(adventureModule.fullName, adventureModule.prefix);

    let importer = new ModuleImporter();
    await importer.import(adventureModule, parsedPdf, output);
}
