import * as PDFJS from "pdfjs-dist";
import * as Foundry from './foundry';
const Diff = require('diff');

import Pica from 'pica';

let pica = new Pica();

function rot13(str) {
    var input     = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    var output    = 'NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm';
    return str.split('').map(x => { const i = input.indexOf(x); return i > -1 ? output[i] : x; }).join('');
}

export interface TextDetail {
    text: string;
    fontId: string;
    fontName: string;
    fontSize: number;
    color: string;
    isLeftAligned: boolean;
    isIndented: boolean;
    page: number;
    x: number;
    y: number;
}

export class PdfImage {
    page: number;
    width: number;
    height: number;
    getImageCanvas: (width?: number, height?: number, cropLeft?: number, cropTop?: number, cropRight?: number, cropBottom?: number, rotateCW?: number) => any;
    getImage: (width?: number, height?: number, cropLeft?: number, cropTop?: number, cropRight?: number, cropBottom?: number, rotateCW?: number) => any;
}

export class PdfData {
    imageDetails: PdfImage[];
    textDetails: TextDetail[];
}

function log(x: string) {
    // let p = document.createElement('p');
    // p.innerText = x;
    // document.body.appendChild(p);
}

export async function parsePdf(file: string|Uint8Array, progressFn: (description: string, pct: number) => void, pages?: number[]): Promise<PdfData> {
    var loadingTask = PDFJS.getDocument(file);
    let pdf = await loadingTask.promise;

    let textDetails : TextDetail[] = [];
    let textDetails2 : TextDetail[] = [];
    let imageDetails : PdfImage[] = [];

    if (pages === undefined || pages.length == 0) pages = Array(pdf.numPages).fill(1).map((i,j) => i+j);
    for (var pagesIndex=0; pagesIndex<pages.length; pagesIndex++) {
        const pageNum = pages[pagesIndex];
        progressFn(`Fetching page ${pageNum}`, pagesIndex / pages.length);

        let page = await pdf.getPage(pageNum);
        let ops = await page.getOperatorList();

        let image_indicies = [];

        let getFontName = (fontName: string) => {
            try {
                let fontDetails = page.commonObjs.get(fontName);
                fontName = fontDetails.name;
            } catch {}

            let fontPlus = fontName.indexOf('+');
            if (fontPlus !== -1)
                fontName = fontName.substring(fontPlus+1);
            
            return fontName;
        };

        // let textContent = await page.getTextContent();
        // for (var item of textContent.items) {
        //     if (item.height < 4 || item.str.trim() == '') {
        //         continue;
        //     }

        //     let str = item.str.replace(/ +/g, ' ');

        //     // let fontName = item.fontName;
        //     // try {
        //     //     let fontDetails = page.commonObjs.get(fontName);
        //     //     fontName = fontDetails.name;
        //     // } catch {}

        //     // let fontPlus = fontName.indexOf('+');
        //     // if (fontPlus !== -1) fontName = fontName.substring(fontPlus+1);
        //     let fontName = getFontName(item.fontName);
        //     if (fontName == 'Times') fontName = 'Times-Italic';
        //     let fontId = fontName + ':' + item.height;

        //     //console.log(`([${item.transform[4]}, ${item.transform[5]}], ${fontName}:${item.height} - #000000) - ${str}`);
        //     textDetails2.push({
        //         text: str,
        //         fontId: fontId,
        //         fontName: fontName,
        //         fontSize: item.height,
        //         isLeftAligned: false,
        //         isIndented: false,
        //         page: page.pageNumber,
        //         x: item.transform[4],
        //         y: item.transform[5],
        //     });
        // }
        // //console.log('-------');

        const opIndexToOp = Object.entries(PDFJS.OPS as Record<string, number>).reduce((ret, entry) => {
            const [key,value] = entry;
            ret[value] = key;
            return ret;
        }, {});

        let defaultState = {
            fontName: '',
            baseFontSize: 1,
            currentTextMatrix: [1, 0, 0, 1, 0, 0],
            fillRGBColor: '',
            leadingMoveText: { x: 0, y: 0 },
            charSpacing: 0,
            wordSpacing: 0,
        };

        let states = [];
        let state = defaultState;
        let currentTexts : TextDetail[] = [];

        for (var i=0; i < ops.fnArray.length; i++) {
            const args = ops.argsArray[i];

            switch (ops.fnArray[i]) {
                case PDFJS.OPS.setTextMatrix:
                    log("    setTextMatrix " + JSON.stringify(args));
                    state.currentTextMatrix = args;
                    break;
                case PDFJS.OPS.setFont:
                    [state.fontName, state.baseFontSize] = args;
                    state.fontName = getFontName(state.fontName);
                    break;
                case PDFJS.OPS.showText:
                    const text = args[0].map(x => x.unicode).join('');
                    const xs = state.currentTextMatrix[2];
                    const ys = state.currentTextMatrix[3];
                    const fontHeightScale = Math.sqrt(xs*xs + ys*ys);
                    const fontSize = state.baseFontSize*fontHeightScale;
                    if (fontSize == 0) {
                        console.log(`${state.baseFontSize} * ${fontHeightScale}`);
                    }

                    if (fontSize > 4 && text.trim() != '') {
                        const fontId = state.fontName + ':' + fontSize;
                        const x = state.currentTextMatrix[4];
                        const y = state.currentTextMatrix[5];
                        log(`([${x}, ${y}], ${state.fontName}:${fontSize} - ${state.fillRGBColor} [${state.charSpacing}]) - ${text}`);

                        if ((state.charSpacing > 0.1 || state.wordSpacing > 0.1) && currentTexts.length > 0 &&
                            state.fontName == currentTexts[currentTexts.length - 1].fontName) {
                            currentTexts[currentTexts.length - 1].text += ' ' + text;
                        } else {
                            currentTexts.push({
                                text: text,
                                fontId: fontId,
                                fontName: state.fontName,
                                fontSize: fontSize,
                                color: state.fillRGBColor,
                                isLeftAligned: false,
                                isIndented: false,
                                page: page.pageNumber,
                                x: x,
                                y: y,
                            });
                        }
                    }
                    break;
                case PDFJS.OPS.beginText:
                    log("    beginText");
                    break;
                case PDFJS.OPS.endText:
                    log("    endText");
                    // if (currentTexts.length > 0) {
                    //     currentTexts[0].x += state.leadingMoveText.x;
                    //     currentTexts[0].y += state.leadingMoveText.y;
                    // }
                    for (const text of currentTexts) {
                        textDetails.push(text);
                    }
                    currentTexts = [];
                    break;
                case PDFJS.OPS.moveText:
                    log("    moveText " + args);
                    state.currentTextMatrix[4] += args[0] * state.currentTextMatrix[0];
                    state.currentTextMatrix[5] += args[1] * state.currentTextMatrix[3];
                    break;
                case PDFJS.OPS.setCharSpacing:
                    state.charSpacing = args[0];
                    break;
                case PDFJS.OPS.setWordSpacing:
                    state.wordSpacing = args[0];
                    break;
                case PDFJS.OPS.setLeadingMoveText:
                    log("    setLeadingMoveText " + JSON.stringify(args));
                    state.leadingMoveText.x = args[0] * state.currentTextMatrix[0];
                    state.leadingMoveText.y = args[1] * state.currentTextMatrix[3];
                    state.currentTextMatrix[4] += state.leadingMoveText.x;
                    state.currentTextMatrix[5] += state.leadingMoveText.y;
                    break;
                case PDFJS.OPS.nextLine:
                    log("    nextLine");
                    state.currentTextMatrix[4] += state.leadingMoveText.x;
                    state.currentTextMatrix[5] += state.leadingMoveText.y;
                    break;

                case PDFJS.OPS.save:
                    states.push(state);
                    break;
                case PDFJS.OPS.restore:
                    state = states.pop();
                    break;

                case PDFJS.OPS.paintJpegXObject:
                case PDFJS.OPS.paintImageXObject:
                    image_indicies.push(args[0]);
                    break;

                case PDFJS.OPS.setFillRGBColor:
                    state.fillRGBColor = '#' + Array.from(args).map((x: number) => x.toString(16).padStart(2, '0')).join('');
                    break;

                default:
                    const opName = opIndexToOp[ops.fnArray[i]];
                    log("    " + opName + " - " + JSON.stringify(args));
                    break;
            }
        }

        for (var obj of image_indicies) {
            const promise = new Promise(resolve => page.objs.get(obj, (response: any) => resolve(response)));
            const data : any = await promise;

            let renderToCanvasFn = async (ctx: CanvasRenderingContext2D) => {};

            if (data.src !== undefined) {
                renderToCanvasFn = async (ctx) => {
                    const img = document.createElement('img');
                    img.src = data.src;
                    const loadedPromise = new Promise(r => img.onload = () => r());
                    document.body.appendChild(img);
                    await loadedPromise;

                    ctx.drawImage(img, 0, 0, data.width, data.height);

                    document.body.removeChild(img);
                };
            } else if (data.kind == 3) {
                renderToCanvasFn = async (ctx) => {
                    var destImageData = ctx.createImageData(data.width, data.height);
                    var dest = destImageData.data;
                    dest.set(data.data, 0);
                    
                    ctx.putImageData(destImageData, 0, 0);
                };
            }

            const getImageCanvas = async (resizeWidth?: number, resizeHeight?: number, cropLeft?: number, cropTop?: number, cropRight?: number, cropBottom?: number, rotateCW?: number) => {
                let canvas = document.createElement("canvas");
                canvas.width = data.width;
                canvas.height = data.height;
                const ctx = canvas.getContext('2d');

                await renderToCanvasFn(ctx);

                if (cropLeft !== undefined && cropTop !== undefined &&
                    cropRight !== undefined && cropBottom !== undefined) {
                    const croppedCanvas = document.createElement("canvas");
                    const cropWidth = cropRight - cropLeft;
                    const cropHeight = cropBottom - cropTop;
                    croppedCanvas.width = cropWidth;
                    croppedCanvas.height = cropHeight;
                    const cropCtx = croppedCanvas.getContext('2d');
                    cropCtx.fillStyle = 'white';
                    cropCtx.fillRect(0, 0, croppedCanvas.width, croppedCanvas.height);
                    cropCtx.drawImage(canvas, cropLeft, cropTop, cropWidth, cropHeight, 0, 0, cropWidth, cropHeight);
                    canvas = croppedCanvas;
                }

                if (rotateCW !== undefined && rotateCW !== 0) {
                    const rotatedCanvas = document.createElement("canvas");
                    let width = canvas.width;
                    let height = canvas.height;
                    if (rotateCW % 2 != 0) {
                        [width, height] = [height, width];
                    }
                    rotatedCanvas.width = width;
                    rotatedCanvas.height = height;
                    const rotatedCtx = rotatedCanvas.getContext('2d');
                    rotatedCtx.fillStyle = 'white';
                    rotatedCtx.fillRect(0, 0, rotatedCanvas.width, rotatedCanvas.height);
                    rotatedCtx.save();
                    rotatedCtx.translate(width/2, height/2);
                    rotatedCtx.rotate(-rotateCW * Math.PI / 2);
                    rotatedCtx.drawImage(canvas, -canvas.width/2, -canvas.height/2);
                    rotatedCtx.restore();
                    canvas = rotatedCanvas;
                }

                if (resizeWidth !== undefined && resizeHeight !== undefined &&
                    resizeWidth != data.width && resizeHeight != data.height) {
                    const finalCanvas = document.createElement("canvas");
                    finalCanvas.width = resizeWidth;
                    finalCanvas.height = resizeHeight;
                    let finalCtx = finalCanvas.getContext('2d');
                    finalCtx.fillStyle = 'white';
                    finalCtx.fillRect(0, 0, finalCanvas.width, finalCanvas.height);
                    canvas = await pica.resize(canvas, finalCanvas);
                }

                // canvas.style.cssText = "border: solid 1px black; background: #999";
                // document.body.appendChild(canvas);

                return canvas;
            };

            imageDetails.push({
                page: page.pageNumber,
                width: data.width,
                height: data.height,
                getImageCanvas: getImageCanvas,
                getImage: async (resizeWidth?: number, resizeHeight?: number, cropLeft?: number, cropTop?: number, cropRight?: number, cropBottom?: number, rotateCW?: number) => {
                    let canvas = await getImageCanvas(resizeWidth, resizeHeight, cropLeft, cropTop, cropRight, cropBottom, rotateCW);
                    return await pica.toBlob(canvas, 'image/png');
                }
            });                
        }
    }

    // for (let i = 0; i<Math.min(textDetails.length, textDetails2.length); i++) {
    //     const lhs = textDetails[i];
    //     const rhs = textDetails2[i];
    //     if (JSON.stringify(lhs) != JSON.stringify(rhs)) {
    //         //console.log(`${i} ${lhs.fontId} != ${rhs.fontId}`);
    //         console.log('----' + i);
    //         console.log(`([${lhs.x}, ${lhs.y}], ${lhs.fontName}:${lhs.fontSize}) - "${lhs.text}"`);
    //         console.log(`([${rhs.x}, ${rhs.y}], ${rhs.fontName}:${rhs.fontSize}) - "${rhs.text}"`);
    //     }
    // }

    // const lhs = textDetails.map(x => `([${x.x}, ${x.y}, ${x.page}], ${x.fontName}:${x.fontSize}) - "${x.text}"`).join('\n');
    // const rhs = textDetails2.map(x => `([${x.x}, ${x.y}, ${x.page}], ${x.fontName}:${x.fontSize}) - "${x.text}"`).join('\n');
    // const diff = Diff.diffChars(lhs, rhs);
    // const fragment = document.createElement('pre');
    // diff.forEach((part) => {
    //     const color = part.added ? 'lightgreen' :
    //         part.removed ? 'red' : 'grey';
    //     const span = document.createElement('span');
    //     span.style.color = color;
    //     span.appendChild(document
    //         .createTextNode(part.value));
    //     fragment.appendChild(span);
    // });
    // document.body.appendChild(fragment);

    // for (let i = 0; i < textDetails.length; i++) {
    //     textDetails[i].text = rot13(textDetails[i].text);
    // }

    return { textDetails, imageDetails };
}

