type ItemTypes = 'action'|'feat'|'melee'|'lore'|'equipment'|'spell';

export interface IItem {
    _id: string;
    name: string;
    img: string;
    type: ItemTypes;
    data: object;
    folder: string;
}

export class Item<T> {
    _id: string;
    name: string = '';
    img: string = 'icons/svg/mystery-man.svg';
    type: ItemTypes;
    data: T;
    folder: string;
    flags: any = {};
}

export class DamageInfo {
    damage: string;
    damageType: string;
}

export class ActionItem extends Item<ActionItemDetails> {
    constructor() {
        super();
        this.type = 'action';
        this.data = new ActionItemDetails();
    }
}

export class MeleeItem extends Item<MeleeItemDetails> {
    constructor() {
        super();
        this.type = 'melee';
        this.data = new MeleeItemDetails();
    }
}

export class LoreItem extends Item<LoreItemDetails> {
    constructor() {
        super();
        this.type = 'lore';
        this.data = new LoreItemDetails();
    }
}

export class EquipmentItem extends Item<EquipmentItemDetails> {
    constructor() {
        super();
        this.type = 'equipment';
        this.data = new EquipmentItemDetails();
    }
}

export class FeatItem extends Item<FeatItemDetails> {
    constructor() {
        super();
        this.type = 'feat';
        this.data = new FeatItemDetails();
        this.img = 'systems/pf2e/icons/features/feats/feats.jpg';
    }
}

export class SpellItem extends Item<SpellItemDetails> {
    constructor() {
        super();
        this.type = 'spell';
        this.data = new SpellItemDetails();
    }
}

export class ActionItemDetails {
    description: {
        value: string;
    } = { value: "" };
    source: {
        value: string;
    } = { value: "" };
    traits: {
        value: string[],
        custom: string;
    } = { value: [], custom: "" };
    actionType: {
        value: "action"|"reaction"|"free"|"passive";
    } = { value: "reaction" };
    actions: { value: string; } = { value: "" }
}

export class MeleeItemDetails {
    description: {
        value: string;
    } = { value: "" };
    source: {
        value: string;
    } = { value: "" };
    traits: {
        value: string[],
        custom: string;
    } = { value: [], custom: "" };
    bonus: { value: number; } = { value: 0 };
    damageRolls: DamageInfo[] = [];
    attackEffects: {
        value: string[];
        custom: string;
    } = { value: [], custom: "" };
    weaponType: {
        value: "melee"|"ranged";
    } = { value: "melee" };
}

export class LoreItemDetails {
    description: { value: string; }
    mod: { value: number }

    constructor() {
        Object.assign(this, {
            description: { value: '' },
            mod: { value: 0 }            
        } as LoreItemDetails);
    }
}

export class EquipmentItemDetails {
    description: { value: string; }
    quantity: { value: number; }

    constructor() {
        Object.assign(this, {
            description: { value: '' },
            quantity: { value: 1 }
        } as EquipmentItemDetails);
    }
}

export class FeatItemDetails {
    featType: {
        value: 'ancestry'|'ancestryfeature'|'archetype'|'bonus'|'class'|'classfeature'|'skill'|'general'|'pfsboon'|'dietyboon'|'curse'
    } = { value: 'bonus' }
    actionType: {
        value: string
    } = { value: '' }
    actionCategory: {
        value: string
    } = { value: '' }
    actions: {
        value: string
    } = { value: '' }
    prerequisites: {
        value: string
    } = { value: '' }

    description: {
        value: string;
        chat: string;
        unidentified: string;
    } = { value: '', chat: '', unidentified: '' };
    source: {
        value: string;
    } = { value: '' };
    traits: {
        value: string[];
    } = { value: [] };
    rarity: {
        value: 'common' | 'uncommon' | 'rare' | 'unique';
    } = { value: 'common' };
    usage: {
        value: string;
    } = { value: '' };

    level: {
        value: number
    } = { value: 0 }
}

export class SpellItemDetails {
    spellType: {
        value: string
    } = { value: '' }
    spellCategorie: {
        value: string
    } = { value: '' }
    traditions: {
        value: []
    } = { value: [] }
    school: {
        value: string
    } = { value: '' }
    components: {
        value: string
    } = { value: '' }
    materials: {
        value: string
    } = { value: '' }
    target: {
        value: string
    } = { value: '' }
    range: {
        value: string
    } = { value: '' }
    area: {
        value: string
    } = { value: '' }
    time: {
        value: string
    } = { value: '' }
    duration: {
        value: string
    } = { value: '' }
    damage: {
        value: string,
        applyMod: false
    } = { value: '', applyMod: false }
    damageType: {
        value: string
    } = { value: '' }
    scaling: {
        mode: string,
        formula: string
    } = { mode: '', formula: '' }
    save: {
        basic: string
        value: string
    } = { basic: '', value: '' }
    sustained: {
        value: false
    } = { value: false }
    cost: {
        value: string
    } = { value: '' }
    ability: {
        value: string
    } = { value: '' }
    prepared: {
        value: boolean
    } = { value: false }
    location: {
        value: string
    } = { value: '' }

    description: {
        value: string;
    } = { value: "" }
    source: {
        value: string;
    } = { value: "" }
    traits: {
        value: string[],
        custom: string;
    } = { value: [], custom: "" }
    rarity: {
        value: 'common' | 'uncommon' | 'rare' | 'unique';
    } = { value: 'common' }
    usage: {
        value: string;
    } = { value: '' }

    level: {
        value: number
    } = { value: 0 }
}
