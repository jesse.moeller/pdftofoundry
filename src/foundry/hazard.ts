
import * as Actor from './actor';

export class HazardActor extends Actor.Actor<HazardDetails> {
    constructor() {
        super();
        this.type = 'hazard';
        this.data = new HazardDetails();
    }
}

export class HazardAttributes {
    hasHealth: boolean = false;
    ac: {
        value: number;
    } = { value: 10 };
    hp: {
        value: number;
        max: number;
        details: string;
    } = { value: 0, max: 0, details: '' };
    hardness: number = 0;
    stealth: {
        value: number;
        details: string;
    } = { value: 0, details: "" };
};

export class HazardDetails {
    source: {
        value: string;
    } = { value: "" };
    flavourText: string = "";
    nethysUrl: string = "";
    recallKnowledgeText: string = "";
    sidebarText: string = "";
    saves: {
        fortitude: Actor.SaveDetails;
        reflex: Actor.SaveDetails;
        will: Actor.SaveDetails;
    } = { fortitude: new Actor.SaveDetails(), reflex: new Actor.SaveDetails(), will: new Actor.SaveDetails() };
    attributes: HazardAttributes = new HazardAttributes();
    details: {
        isComplex: boolean;
        level: number;
        disable: string;
        description: string;
        reset: string;
        routine: string;
    } = { isComplex: false, level: 0, disable: '', description: '', reset: '', routine: '' };
    traits: {
        traits: {
            value: string[];
            custom: string;
        },
        di: { custom: string, value: string[] },
        dr: any[];
        dv: any[];
        ci: any[];
    } = { traits: { value: [], custom: "" }, di: { custom: "", value: [] }, dr: [], dv: [], ci: [] };
}
