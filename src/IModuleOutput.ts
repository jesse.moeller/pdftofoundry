import * as Foundry from './foundry'

export interface IModuleOutput {
    progressFn(arg0: string, arg1: number);

    emitFile(path: string, name: string, data: string | Blob): Promise<string>

    createScene(scene: object): Promise<void>;

    createJournal(journal: Foundry.Journal): Promise<void>;
    
    createActor(actor: Foundry.IActor): Promise<void>;

    createItem(item: Foundry.IItem): Promise<void>;
}