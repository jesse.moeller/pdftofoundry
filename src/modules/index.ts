import crbModule from './crb'
import gmgModule from './gmg'
import apgModule from './apg'
import fallofplaguestone from './fallofplaguestone'
import slithering from './slithering'
import ageOfAshes1 from './ageofashes1'
import ageOfAshes2 from './ageofashes2'
import ageOfAshes3 from './ageofashes3'
import ageOfAshes4 from './ageofashes4'
import ageOfAshes5 from './ageofashes5'
import ageOfAshes6 from './ageofashes6'
import extinctioncurse1 from './extinctioncurse1'
import extinctioncurse2 from './extinctioncurse2'
import extinctioncurse3 from './extinctioncurse3'
import extinctioncurse4 from './extinctioncurse4'
import extinctioncurse5 from './extinctioncurse5'
import extinctioncurse6 from './extinctioncurse6'
import agentsOfEverwatch1 from './aoe1'
import pfs1_01 from './pfs1.01'
import pfs1_05 from './pfs1.05'
import pfs1_06 from './pfs1.06'
import pfs1_12 from './pfs1.12'
import pfs1_16 from './pfs1.16'
import pfs1_17 from './pfs1.17'
import pfsq2 from './pfsq2'
import pfsq3 from './pfsq3'
import pfsq10 from './pfsq10'

export default [
    crbModule,
    gmgModule,
    apgModule,
    fallofplaguestone,
    slithering,
    ageOfAshes1,
    ageOfAshes2,
    ageOfAshes3,
    ageOfAshes4,
    ageOfAshes5,
    ageOfAshes6,
    extinctioncurse1,
    extinctioncurse2,
    extinctioncurse3,
    extinctioncurse4,
    extinctioncurse5,
    extinctioncurse6,
    agentsOfEverwatch1,
    pfs1_01,
    pfs1_05,
    pfs1_06,
    pfs1_12,
    pfs1_16,
    pfs1_17,
    pfsq2,
    pfsq3,
    pfsq10
];