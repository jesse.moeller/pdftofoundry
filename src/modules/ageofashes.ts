import { ModuleDescription, ModuleData } from "./IAdventure";
import { ModuleScene } from "./ModuleScene";
import { PdfData, TextDetail } from "../parse";
import { PageLayoutEmitFunction, PageLayout } from "./PageLayouts";
import { Journal } from "../foundry";
import { ParserText, makeContiguousParser } from "./parser";
import { parseMonsterOrHazard } from "./core";

export class ChapterHeadingPageLayout extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        const headerBlocks = pdf.textDetails
            .filter(t => t.page == this.pageNum && t.fontName == 'RomicStd-Bold' && t.fontSize > 30)
            .map(t => t.y);
        const headingStartY = Math.min(...headerBlocks) + 10;

        this.getAllTextInBoundingBox(pdf, 0, 1000, 1000, headingStartY, emit);
        this.getAllTextInBoundingBox(pdf, 0, 300, headingStartY, 40, emit);
        this.getAllTextInBoundingBox(pdf, 325, 1000, headingStartY, 40, emit);
    }
};

export class RightSingleColumnWithSidebar extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 67.5, 370, 700, 40, emit);
        this.getAllTextInBoundingBox(pdf, 370, 510, 700, 40, emit, 'sidebar'+this.pageNum);
    }
};

export class LeftTwoColumn extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 99, 320, 700, 40, emit);
        this.getAllTextInBoundingBox(pdf, 326.25, 550, 700, 40, emit);
    }
};

export class RightTwoColumn extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 67.5, 290, 700, 40, emit);
        this.getAllTextInBoundingBox(pdf, 294.75, 515, 700, 40, emit);
    }
};

export class LeftTwoColumnWithMap extends PageLayout {
    sectionTop: number;
    
    constructor(pageNum: number, sectionTop?: number) {
        super(pageNum);
        this.sectionTop = sectionTop ?? 327;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 99, 320, this.sectionTop, 40, emit);
        this.getAllTextInBoundingBox(pdf, 326.25, 550, this.sectionTop, 40, emit);
    }
};

export class RightTwoColumnWithRightCutout extends PageLayout {
    cutoutPos: number;
    section?: string;

    constructor(pageNum: number, cutoutPos: number, section?: string) {
        super(pageNum);
        this.cutoutPos = cutoutPos;
        this.section = section;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 67.5, 290, 700, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 294.75, 515, this.cutoutPos, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 294.75, 515, 700, this.cutoutPos, emit, 'sidebar'+this.pageNum);
    }
};


const parseIcon = (p: ParserText) => {
    return p.consume().text;
};

const parseItalic = makeContiguousParser("<i>", "</i>");
const parseBold = makeContiguousParser("<b>", "</b>");
const parseHeading = makeContiguousParser("<h3>", "</h3>");

const headingFontIds = ['GoodOT-News:12', 'GoodOT:12'];
const normalFontIds = ['SabonLTStd-Roman:9', 'Sabon-Roman:9'];
const italicFontIds = ['SabonLTStd-Italic:9', 'Sabon-Italic:9'];
const boldFontIds = ['TimesNewRomanPS-BoldMT:9', 'TimesNewRomanPSMT:9', 'Sabon-Bold:9'];

const paragraphFontIds = headingFontIds
    .concat(normalFontIds)
    .concat(italicFontIds)
    .concat(boldFontIds);

const parseParagraph = (p: ParserText) => {
    let current = '';

    while (p.anyRemaining()) {
        const peekFont = p.peek().fontId;
        if (normalFontIds.includes(peekFont)) {
            current += p.consume().text;
        } else if (headingFontIds.includes(peekFont)) {
            current += parseHeading(p);
        } else if (italicFontIds.includes(peekFont)) {
            current += parseItalic(p);
        } else if (boldFontIds.includes(peekFont)) {
            current += parseBold(p);
        } else {
            break;
        }

        if (p.anyRemaining() && p.peek().isIndented) {
            break;
        }
    }
    return "<p>" + current + "</p>";
};

const parseBlockquote = (p: ParserText) => {
    let current = '';
    while (p.anyRemaining()) {
        if (p.peek().fontId == 'GoodOT:9') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'GoodOT-Italic:9') {
            current += parseItalic(p);
        } else if (p.peek().fontId == 'GoodOT-Bold:9') {
            current += parseBold(p);
        } else if (p.peek().fontId == 'Almagro:9') {
            current += p.consume().text;
        } else {
            break;
        }
    }
    return "<blockquote>" + current + "</blockquote>";
};

const parseList = (p: ParserText) => {
    let current = '';

    p.consume();
    
    while (p.anyRemaining()) {
        const peekFont = p.peek().fontId;
        if (normalFontIds.includes(peekFont)) {
            current += p.consume().text;
        } else if (headingFontIds.includes(peekFont)) {
            current += parseHeading(p);
        } else if (italicFontIds.includes(peekFont)) {
            current += parseItalic(p);
        } else if (boldFontIds.includes(peekFont)) {
            current += parseBold(p);
        } else {
            break;
        }

        if (p.anyRemaining() && p.peek().isIndented) {
            break;
        }
    }
    return "<li>" + current + "</li>";
};

const parseAside = (p: ParserText) => {
    return new Journal(p.consume().text, parseBlockquote(p));
};

const parseSection = (p: ParserText, moduleData: ModuleData) => {
    let current = '';

    let name = p.consume().text;
    if (p.peek().fontId == 'GoodOT-Bold:14') {
        name += "(" + p.consume().text + ")";
    }

    const headingFonts = ['Taroca:16', 'GoodOT-Bold:12'];
    while (headingFonts.includes(p.peek().fontId)) {
        name += p.consume().text;
    }

    while (p.anyRemaining()) {
        let cur = p.peek();
        if (cur.fontId == 'GoodOT:9' && cur.text == '•') {
            current += parseList(p);
        } else if (cur.fontId == 'GoodOT:9' || cur.fontId == 'GoodOT-Bold:9') {
            current += parseBlockquote(p);
        } else if (cur.fontId == 'GoodOT-CondBold:12') {
            current += parseMonsterOrHazard(p, moduleData);
        } else if (cur.fontId == 'GoodOT-Bold:11') {
            moduleData.journalEntries.push(parseAside(p));
        } else if (cur.fontId == 'GoodOT-Bold:12' && !(cur.text.match(/^[ABCDEFG]\d+\./) || cur.text.match(/^Event \d+:/))) {
            current += parseHeading(p);
        } else if (paragraphFontIds.includes(cur.fontId)) {
            current += parseParagraph(p);
        } else {
            //console.log(`abandoning section on: ${cur.fontId} :: (${cur.x}, ${cur.y}) :: ${cur.text}`);
            break;
        }
    }

    moduleData.journalEntries.push(new Journal(name, current));
};

export function extractDetails(text: Array<TextDetail>, moduleData: ModuleData) {
    const blacklistedFonts = ['Times-Italic:8', 'RomicStd-Bold:12', 'TradeGothicLTStd-BdCn20:12', 'TradeGothicLTStd-BdCn20:8', 'Taroca:12', 'TradeGothicLTStd-BdCn20:11.0714']
    text = text.filter(t => !blacklistedFonts.includes(t.fontId));

    let p = new ParserText(text);

    const headingFonts = ['GoodOT-Bold:14', 'RomicStd-Bold:14', 'RomicStd-Bold:32', 'RomicStd-Bold:33', 'RomicStd-Bold:38', 'RomicStd-Bold:40', 'GoodOT-Bold:11'];

    while (p.anyRemaining()) {
        let cur = p.peek();

        if (headingFonts.includes(cur.fontId)) {
            //console.log("starting section: " + cur.text);
            parseSection(p, moduleData);
        } else if (cur.fontId == 'GoodOT-Bold:11') {
            moduleData.journalEntries.push(parseAside(p));
        } else {
            let item = p.consume();
            console.log(`unknown ${item.fontId} :: ${item.page} (${item.x}, ${item.y}) :: ${item.text}`);
        }
    }
}