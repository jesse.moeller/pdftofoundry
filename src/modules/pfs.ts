import { ModuleDescription, ModuleData } from "./IAdventure";
import { ModuleScene } from "./ModuleScene";
import { PdfData, TextDetail } from "../parse";
import { PageLayoutEmitFunction, PageLayout } from "./PageLayouts";
import { Journal } from "../foundry";
import { ParserText, makeContiguousParser } from "./parser";
import { parseMonsterOrHazard } from "./core";

export class TwoColumn extends PageLayout {
    section?: string;
    constructor(pageNum: number, section?: string) {
        super(pageNum);
        this.section = section;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 72, 300, 700, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 312, 550, 700, 40, emit, this.section);
    }
};

export class TwoColumnWithLeftCutout extends PageLayout {
    cutoutPos: number;
    section?: string;

    constructor(pageNum: number, cutoutPos: number, section?: string) {
        super(pageNum);
        this.cutoutPos = cutoutPos;
        this.section = section;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 72, 300, this.cutoutPos, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 312, 550, 700, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 72, 300, 700, this.cutoutPos, emit, 'sidebar'+this.pageNum);
    }
};

export class TwoColumnWithRightCutout extends PageLayout {
    cutoutPos: number;
    section?: string;

    constructor(pageNum: number, cutoutPos: number, section?: string) {
        super(pageNum);
        this.cutoutPos = cutoutPos;
        this.section = section;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 72, 300, 700, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 312, 550, this.cutoutPos, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 312, 550, 700, this.cutoutPos, emit, 'sidebar'+this.pageNum);
    }
};

export class TwoColumnWithMap extends PageLayout {
    cutoutPos: number;
    section?: string;

    constructor(pageNum: number, cutoutPos: number, section?: string) {
        super(pageNum);
        this.cutoutPos = cutoutPos;
        this.section = section;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 72, 300, this.cutoutPos, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 312, 550, this.cutoutPos, 40, emit, this.section);
    }
};

export class TwoColumnWithBottomCutout extends PageLayout {
    cutoutPos: number;
    section?: string;

    constructor(pageNum: number, cutoutPos: number, section?: string) {
        super(pageNum);
        this.cutoutPos = cutoutPos;
        this.section = section;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 72, 300, 700, this.cutoutPos, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 312, 550, 700, this.cutoutPos, emit, this.section);
    }
};

const parseIcon = (p: ParserText) => {
    return p.consume().text;
};

const parseItalic = makeContiguousParser("<i>", "</i>");
const parseBold = makeContiguousParser("<b>", "</b>");
const parseHeading = makeContiguousParser("<h3>", "</h3>");

const headingFontIds = ['GoodOT-News:12', 'GoodOT:12'];
const normalFontIds = ['SabonLTStd-Roman:9', 'Sabon-Roman:9', 'NexusSerifOT:9'];
const italicFontIds = ['SabonLTStd-Italic:9', 'NexusSerifOT-Italic:9'];
const boldFontIds = ['TimesNewRomanPS-BoldMT:9', 'TimesNewRomanPSMT:9', 'SabonLTStd-Bold:9', 'GoodOT-Bold:10', 'Dax-Bold:9'];

const paragraphFontIds = headingFontIds
    .concat(normalFontIds)
    .concat(italicFontIds)
    .concat(boldFontIds);

const parseParagraph = (p: ParserText) => {
    let current = '';

    while (p.anyRemaining()) {
        const peekFont = p.peek().fontId;
        if (normalFontIds.includes(peekFont)) {
            current += p.consume().text;
        } else if (headingFontIds.includes(peekFont)) {
            current += parseHeading(p);
        } else if (italicFontIds.includes(peekFont)) {
            current += parseItalic(p);
        } else if (boldFontIds.includes(peekFont)) {
            current += parseBold(p);
        } else {
            break;
        }

        if (p.anyRemaining() && p.peek().isIndented) {
            break;
        }
    }
    return "<p>" + current + "</p>";
};

const parseBlockquote = (p: ParserText) => {
    let current = '';
    while (p.anyRemaining()) {
        if (p.peek().fontId == 'GoodOT:9') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'GoodOT-Italic:9') {
            current += parseItalic(p);
        } else if (p.peek().fontId == 'GoodOT-Bold:9' || p.peek().fontId == 'GoodOT-Bold:10') {
            current += parseBold(p);
        } else if (p.peek().fontId == 'Almagro:9') {
            current += p.consume().text;
        } else {
            break;
        }
    }
    return "<blockquote>" + current + "</blockquote>";
};

const parseList = (p: ParserText) => {
    let current = '';

    p.consume();
    
    while (p.anyRemaining()) {
        const peekFont = p.peek().fontId;
        if (normalFontIds.includes(peekFont)) {
            current += p.consume().text;
        } else if (headingFontIds.includes(peekFont)) {
            current += parseHeading(p);
        } else if (italicFontIds.includes(peekFont)) {
            current += parseItalic(p);
        } else if (boldFontIds.includes(peekFont)) {
            current += parseBold(p);
        } else {
            break;
        }

        if (p.anyRemaining() && p.peek().isIndented) {
            break;
        }
    }
    return "<li>" + current + "</li>";
};

const parseAside = (p: ParserText) => {
    let heading = p.consume().text;
    while (p.anyRemaining() && p.peek().fontId == 'GoodOT-Bold:11')
        heading += p.consume().text;

    return new Journal(heading, parseBlockquote(p));
};

const parseSection = (p: ParserText, moduleData: ModuleData) => {
    let current = '';

    let name = p.consume().text;
    if (p.anyRemaining() && p.peek().fontId == 'GoodOT-Bold:14') {
        name += "(" + p.consume().text + ")";
    }

    const headingFonts = ['Taroca:16', 'GoodOT-Bold:12'];
    while (headingFonts.includes(p.peek().fontId)) {
        name += p.consume().text;
    }

    while (p.anyRemaining()) {
        let cur = p.peek();
        if (paragraphFontIds.includes(cur.fontId)) {
            current += parseParagraph(p);
        } else if (cur.fontId == 'GoodOT:9' && cur.text == '•') {
            current += parseList(p);
        } else if (cur.fontId == 'GoodOT:9' || cur.fontId == 'GoodOT-Bold:9') {
            current += parseBlockquote(p);
        } else if (cur.fontId == 'GoodOT-CondBold:12') {
            current += parseMonsterOrHazard(p, moduleData);
        } else if (cur.fontId == 'GoodOT-Bold:11') {
            moduleData.journalEntries.push(parseAside(p));
        } else if (cur.fontId == 'GoodOT-Bold:12' && !(cur.text.match(/^[ABCDEFG]\d+\./) || cur.text.match(/^Event \d+:/))) {
            current += parseHeading(p);
        } else {
            //console.log(`abandoning section on: ${cur.fontId} :: (${cur.x}, ${cur.y}) :: ${cur.text}`);
            break;
        }
    }

    moduleData.journalEntries.push(new Journal(name, current));
};


export function extractDetails(text: Array<TextDetail>, moduleData: ModuleData) {
    const blacklistedFonts = ['Times-Italic', 'RomicStd-Bold:12', 'TradeGothicLTStd-BdCn20:12', 'TradeGothicLTStd-BdCn20:8', 'Taroca:12']
    text = text.filter(t => !blacklistedFonts.includes(t.fontId));

    let p = new ParserText(text);

    while (p.anyRemaining()) {
        let cur = p.peek();
        if (cur.fontId == 'Taroca:16' || cur.fontId == 'GoodOT-Bold:12') {
            //console.log("starting section: " + cur.text);
            parseSection(p, moduleData);
        } else if (cur.fontId == 'GoodOT-Bold:11') {
            moduleData.journalEntries.push(parseAside(p));
        } else {
            let item = p.consume();
            console.log(`unknown ${item.fontId} :: ${item.page} (${item.x}, ${item.y}) :: ${item.text}`);
        }
    }
}
