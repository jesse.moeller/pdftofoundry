import { TextDetail } from "src/parse";


export function makeContiguousParser(pre: string, post: string) {
    return (p: ParserText) => {
        let current = '';
        const fontId = p.peek().fontId;
        while (p.anyRemaining() && p.peek().fontId == fontId) {
            current += p.consume().text;
        }
        if (current.endsWith(" ")) {
            return pre + current.trimRight() + post + " ";
        }
        return pre + current + post;
    };
};

export class ParserText {
    text: TextDetail[];
    i: number;
    bailCount: number;

    constructor(text: TextDetail[]) {
        this.text = text;
        this.i = 0;
        this.bailCount = 0;
    }

    peek(): TextDetail {
        this.bailCount++;
        if (this.i < this.text.length) {
            return this.text[this.i];
        }
        return null;
    }

    consume(): TextDetail {
        this.bailCount = 0;
        if (this.i < this.text.length) {
            return this.text[this.i++];
        }
        return null;
    }

    anyRemaining(): boolean {
        if (this.bailCount > 1000) {
            console.log('bailed out. we done bad :(');
            return false;
        }

        return this.i < this.text.length;
    }
}