import { ModuleDescription, ModuleData } from "./IAdventure";
import { PdfData } from "../parse";
import * as Core from "./core";

const apgModule : ModuleDescription = {
    prefix: "apg",
    fullName: "Advanced Player's Guide",
    detect: (pdf: PdfData) => {
        Core.mergeWithNext(pdf.textDetails, 81, 504, 111);
        Core.mergeWithNext(pdf.textDetails, 89, 509.8903, 615);
        Core.mergeWithNext(pdf.textDetails, 89, 509.8786, 531);
        Core.mergeWithNext(pdf.textDetails, 89, 509.87879999999996, 399);
        Core.mergeWithNext(pdf.textDetails, 89, 508.6311, 255);
        Core.mergeWithNext(pdf.textDetails, 90, 261.1423, 363);
        Core.mergeWithNext(pdf.textDetails, 91, 429.0928, 327);
        Core.mergeWithNext(pdf.textDetails, 109, 409.924, 339);
        Core.mergeWithNext(pdf.textDetails, 110, 498.3224, 219);
        Core.mergeWithNext(pdf.textDetails, 110, 498.3224, 219);
        Core.mergeWithNext(pdf.textDetails, 110, 498.3225, 135);
        Core.mergeWithNext(pdf.textDetails, 111, 268.8221, 615);
        Core.mergeWithNext(pdf.textDetails, 111, 178.7616, 471);
        Core.mergeWithNext(pdf.textDetails, 112, 165.388, 183);
        Core.mergeWithNext(pdf.textDetails, 112, 423.3329, 171);
        Core.mergeWithNext(pdf.textDetails, 112, 409.9174, 531);
        Core.mergeWithNext(pdf.textDetails, 112, 256.0548, 339);
        Core.mergeWithNext(pdf.textDetails, 114, 195.333, 507);
        Core.mergeWithNext(pdf.textDetails, 114, 259.8218, 387);
        Core.mergeWithNext(pdf.textDetails, 119, 270.19010000000003, 327);
        Core.mergeWithNext(pdf.textDetails, 120, 453.3165, 663);
        Core.mergeWithNext(pdf.textDetails, 121, 168.4418, 531);
        Core.mergeWithNext(pdf.textDetails, 122, 255.94639999999998, 135);
        Core.mergeWithNext(pdf.textDetails, 123, 211.0049, 579);
        Core.mergeWithNext(pdf.textDetails, 123, 507.8981, 387);
        Core.mergeWithNext(pdf.textDetails, 124, 189.0092, 483);
        Core.mergeWithNext(pdf.textDetails, 127, 174.1602, 303);
        Core.mergeWithNext(pdf.textDetails, 129, 270.7901, 579);
        Core.mergeWithNext(pdf.textDetails, 130, 255.4188, 687);
        Core.mergeWithNext(pdf.textDetails, 130, 199.4456, 423);
        Core.mergeWithNext(pdf.textDetails, 131, 181.1252, 675);
        Core.mergeWithNext(pdf.textDetails, 132, 251.639, 183);
        Core.mergeWithNext(pdf.textDetails, 136, 108.4975, 543);
        Core.mergeWithNext(pdf.textDetails, 136, 498.9108, 231);
        Core.mergeWithNext(pdf.textDetails, 136, 260.38689999999997, 615);
        Core.mergeWithNext(pdf.textDetails, 136, 498.9107, 135);
        Core.mergeWithNext(pdf.textDetails, 137, 181.6778, 567);
        Core.mergeWithNext(pdf.textDetails, 137, 269.3981, 135);
        Core.mergeWithNext(pdf.textDetails, 137, 507.88650000000007, 651);
        Core.mergeWithNext(pdf.textDetails, 138, 256.6311, 615);
        Core.mergeWithNext(pdf.textDetails, 140, 420.2494, 459);
        Core.mergeWithNext(pdf.textDetails, 181, 212.3008, 99);
        Core.mergeWithNext(pdf.textDetails, 182, 206.79469999999998, 459);
        Core.mergeWithNext(pdf.textDetails, 183, 507.922, 507);
        Core.mergeWithNext(pdf.textDetails, 186, 398.8962, 711);
        Core.mergeWithNext(pdf.textDetails, 189, 407.6081, 603);
        Core.mergeWithNext(pdf.textDetails, 195, 411.7557, 555);
        Core.mergeWithNext(pdf.textDetails, 199, 405.076, 555);
        Core.mergeWithNext(pdf.textDetails, 199, 508.8105, 207);
        Core.mergeWithNext(pdf.textDetails, 200, 478.2938, 495);


        pdf.textDetails.forEach((x, i) => {
            if (x => x.page >= 129 && x.page <= 132 && x.text.startsWith('LEVEL'))
                pdf.textDetails[i].text = x.text.replace('LEVEL', 'FEAT');
        })

        return pdf.textDetails.some(t => t.text == 'Advanced Player’s Guide' && t.fontId == 'Taroca:36');
    },
    detectPageLayout: Core.detectPageLayout,
    extractDetails: Core.extractDetails,
    pages: [
        // catfolk
        new Core.RightChapterHeadingWithSidebarAPG(8),

        new Core.LeftChapterHeading(9),
        new Core.RightOneColumnWithSidebarAPG(10),
        new Core.LeftTwoColumn(11, 76.5),
        new Core.RightOneColumnWithSidebarAPG(12),

        // kobold
        new Core.LeftChapterHeading(13),
        new Core.RightOneColumnWithSidebarAPG(14),
        new Core.LeftTwoColumn(15, 76.5),
        new Core.RightOneColumnWithSidebarAPG(16),

        // orc
        new Core.LeftChapterHeading(17),
        new Core.RightOneColumnWithSidebarAPG(18),
        new Core.LeftTwoColumn(19, 76.5),
        new Core.RightOneColumnWithSidebarAPG(20),

        // ratfolk
        new Core.LeftChapterHeading(21),
        new Core.RightOneColumnWithSidebarAPG(22),
        new Core.LeftTwoColumn(23, 76.5),
        new Core.RightOneColumnWithSidebarAPG(24),

        // tengu
        new Core.LeftChapterHeading(25),
        new Core.RightOneColumnWithSidebarAPG(26),
        new Core.LeftTwoColumn(27, 76.5),
        new Core.RightOneColumnWithSidebarAPG(28),

        // versatile heritages
        new Core.LeftChapterHeading(29),
        new Core.RightOneColumnWithSidebarAPG(30),
        new Core.LeftChapterHeading(31),
        new Core.RightTwoColumn(32),
        new Core.LeftChapterHeading(33),
        new Core.RightTwoColumn(34),
        new Core.LeftChapterHeading(35),
        new Core.RightTwoColumn(36),
        new Core.LeftTwoColumn(37),
        new Core.RightTwoColumn(38),
        new Core.LeftTwoColumn(39),
        new Core.RightTwoColumn(40),
        new Core.LeftTwoColumn(41),
        new Core.RightTwoColumn(42),
        new Core.LeftTwoColumn(43),
        new Core.RightTwoColumn(44),
        new Core.LeftTwoColumn(45),
        new Core.RightTwoColumn(46),
        new Core.LeftTwoColumn(47),
        new Core.RightTwoColumn(48),

        // common backgrounds
        new Core.LeftChapterHeading(49),
        new Core.RightTwoColumn(50),
        
        // rare backgrounds
        new Core.LeftChapterHeading(51),
        new Core.RightTwoColumnWithMap(52),

        // investigator
        new Core.RightChapterHeadingWithSidebar(56),
        new Core.LeftTwoColumn(57),
        new Core.RightTwoColumn(58),
        new Core.LeftOneColumnWithLeftSidebar(59),
        new Core.RightTwoColumn(60),
        new Core.LeftTwoColumn(61),
        new Core.RightTwoColumnWithMap(62),
        new Core.LeftTwoColumnWithLeftSidebar(63),
        new Core.RightTwoColumn(64),
        new Core.LeftTwoColumnWithLeftSidebar(65),
        new Core.RightTwoColumn(66),

        // oracle
        new Core.RightChapterHeadingWithSidebar(68),
        new Core.LeftTwoColumn(69),
        new Core.RightTwoColumn(70),
        new Core.LeftOneColumnWithLeftSidebar(71),
        new Core.RightTwoColumn(72),
        new Core.LeftTwoColumn(73),
        new Core.RightTwoColumnWithMap(74),
        new Core.LeftTwoColumnWithLeftSidebar(75),
        new Core.RightTwoColumn(76),
        new Core.LeftTwoColumnWithLeftSidebar(77),
        new Core.RightTwoColumn(78),
        new Core.LeftTwoColumn(79),
        new Core.RightTwoColumn(80),
        new Core.LeftTwoColumn(81),
        new Core.RightTwoColumn(82),
        
        // swashbuckler
        new Core.RightChapterHeadingWithSidebar(84),
        new Core.LeftTwoColumn(85),
        new Core.RightTwoColumn(86),
        new Core.LeftOneColumnWithLeftSidebar(87),
        new Core.RightTwoColumn(88),
        new Core.LeftTwoColumn(89),
        new Core.RightTwoColumnWithMap(90),
        new Core.LeftTwoColumnWithLeftSidebar(91),
        new Core.RightTwoColumn(92),
        new Core.LeftTwoColumnWithLeftSidebar(93),
        new Core.RightTwoColumn(94),

        // witch
        new Core.RightChapterHeadingWithSidebar(96),
        new Core.LeftTwoColumn(97),
        new Core.RightOneColumnWithSidebar(98),
        new Core.LeftTwoColumn(99),
        new Core.RightTwoColumn(100),
        new Core.LeftOneColumnWithLeftSidebar(101),
        new Core.RightTwoColumnWithRightSidebar(102),
        new Core.LeftTwoColumn(103),
        new Core.RightTwoColumnWithMap(104),
        new Core.LeftTwoColumnWithLeftSidebar(105),
        new Core.RightTwoColumn(106),

        // alchemist
        new Core.LeftChapterHeading(107),
        new Core.RightTwoColumn(108),

        // barbarian
        new Core.LeftChapterHeading(109),
        new Core.RightTwoColumnWithMap(110),
        new Core.LeftTwoColumn(111),
        new Core.RightTwoColumn(112),

        // bard
        new Core.LeftChapterHeading(113),
        new Core.RightTwoColumn(114),
        new Core.LeftTwoColumn(115),
        new Core.RightTwoColumn(116),

        // champion
        new Core.LeftChapterHeading(117),
        new Core.RightTwoColumn(118),
        new Core.LeftTwoColumn(119),
        new Core.RightTwoColumn(120),
        new Core.LeftTwoColumn(121),
        new Core.RightTwoColumn(122),

        // cleric
        new Core.LeftChapterHeading(123),
        new Core.RightTwoColumn(124),

        // druid
        new Core.LeftChapterHeading(125),
        new Core.RightTwoColumnWithMap(126),

        // fighter
        new Core.LeftChapterHeading(127),
        new Core.RightTwoColumn(128),

        // monk
        new Core.LeftChapterHeading(129),
        new Core.RightTwoColumn(130),
        new Core.LeftTwoColumn(131),
        new Core.RightTwoColumnWithMap(132),

        // ranger
        new Core.LeftChapterHeading(133),
        new Core.RightTwoColumnWithRightSidebar(134),

        // rogue
        new Core.LeftChapterHeading(135),
        new Core.RightTwoColumn(136),
        new Core.LeftTwoColumn(137),
        new Core.RightTwoColumn(138),

        // sorcerer
        new Core.LeftChapterHeading(139),
        new Core.RightTwoColumn(140),
        new Core.LeftTwoColumn(141),
        new Core.RightTwoColumn(142),

        // wizard
        new Core.LeftChapterHeading(143),
        new Core.RightTwoColumnWithMap(144),

        // animal companions
        new Core.LeftChapterHeading(145),
        new Core.RightTwoColumn(146),

        // familiars
        new Core.LeftChapterHeading(147),
        new Core.RightTwoColumn(148),

        // archetypes
        new Core.RightChapterHeadingWithSidebarAPG(150),
        new Core.LeftTwoColumn(151),
        new Core.RightTwoColumn(152),
        // 153 art
        new Core.RightTwoColumn(154),
        new Core.LeftTwoColumn(155),
        new Core.RightTwoColumn(156),
        new Core.LeftTwoColumn(157),
        new Core.RightTwoColumn(158),
        new Core.LeftTwoColumn(159),
        new Core.RightTwoColumn(160),
        new Core.LeftTwoColumn(161),
        new Core.RightTwoColumn(162),
        new Core.LeftTwoColumn(163),
        new Core.RightTwoColumn(164),
        new Core.LeftTwoColumn(165),
        new Core.RightTwoColumn(166),
        new Core.LeftTwoColumn(167),
        new Core.RightTwoColumn(168),
        new Core.LeftTwoColumn(169),
        new Core.RightOneColumnWithSidebar(170),
        new Core.LeftTwoColumn(171),
        new Core.RightTwoColumn(172),
        // 173 art
        new Core.RightTwoColumn(174),
        new Core.LeftTwoColumn(175),
        new Core.RightTwoColumn(176),
        new Core.LeftTwoColumn(177),
        new Core.RightTwoColumn(178),
        new Core.LeftTwoColumn(179),
        new Core.RightTwoColumn(180),
        new Core.LeftTwoColumn(181),
        new Core.RightTwoColumn(182),
        new Core.LeftTwoColumn(183),
        new Core.RightTwoColumn(184),
        new Core.LeftTwoColumn(185),
        new Core.RightTwoColumn(186),
        new Core.LeftTwoColumn(187),
        new Core.RightTwoColumn(188),
        new Core.LeftTwoColumn(189),
        new Core.RightTwoColumn(190),
        new Core.LeftTwoColumn(191),
        new Core.RightTwoColumn(192),
        new Core.LeftTwoColumn(193),
        new Core.RightTwoColumn(194),
        new Core.LeftTwoColumn(195),
        new Core.RightTwoColumn(196),
        new Core.LeftTwoColumn(197),
        new Core.RightTwoColumn(198),
        new Core.LeftTwoColumn(199),
        new Core.RightTwoColumn(200),

        // feats
        new Core.RightChapterHeading(202),
        new Core.LeftTwoColumn(203),
        new Core.RightTwoColumn(204),
        new Core.LeftTwoColumn(205),
        new Core.RightTwoColumnWithMap(206),
        new Core.LeftTwoColumn(207),
        new Core.RightTwoColumnWithMap(208),
        new Core.LeftTwoColumn(209),
        new Core.RightTwoColumn(210),

        // spells
        new Core.RightChapterHeadingWithSidebarAPG(212),
        //new Core.LeftTwoColumn(213),
        //new Core.RightOneColumnWithSidebar(214),
        new Core.LeftTwoColumn(215),
        new Core.RightTwoColumnWithMap(216),
        new Core.LeftTwoColumn(217),
        new Core.RightTwoColumn(218),
        new Core.LeftTwoColumn(219),
        new Core.RightTwoColumnWithMap(220),
        new Core.LeftTwoColumn(221),
        new Core.RightTwoColumn(222),
        new Core.LeftTwoColumn(223),
        new Core.RightTwoColumn(224),
        new Core.LeftTwoColumn(225),
        new Core.RightTwoColumnWithMap(226),
        new Core.LeftTwoColumn(227),
        new Core.RightTwoColumn(228),
        new Core.LeftChapterHeading(229),
        new Core.RightTwoColumn(230),
        new Core.LeftTwoColumn(231),
        new Core.RightTwoColumnWithMap(232),
        new Core.LeftTwoColumn(233),
        new Core.RightTwoColumn(234),
        new Core.LeftTwoColumn(235),
        new Core.RightTwoColumn(236),
        new Core.LeftTwoColumn(237),
        new Core.RightTwoColumn(238),
        new Core.LeftTwoColumn(239),
        new Core.RightTwoColumn(240),
        new Core.LeftChapterHeading(241),
        new Core.RightTwoColumn(242),
        new Core.LeftTwoColumn(243, 75),
        new Core.RightTwoColumnWithMap(244),
        new Core.LeftTwoColumn(245),
        new Core.RightTwoColumn(246),

        // items
        new Core.RightChapterHeadingWithSidebarAPG(248),
        new Core.LeftChapterHeading(249),
        new Core.RightTwoColumn(250),
        //new Core.LeftTwoColumn(251),
        //new Core.RightTwoColumn(252),
        new Core.LeftTwoColumn(253),
        new Core.RightTwoColumn(254),
        new Core.LeftTwoColumn(255),
        new Core.RightTwoColumn(256),
        new Core.LeftTwoColumn(257),
        new Core.RightTwoColumn(258),
        new Core.LeftTwoColumn(259),
        new Core.RightTwoColumn(260),
        new Core.LeftTwoColumn(261),
        new Core.RightTwoColumn(262),
        new Core.LeftTwoColumn(263),
        new Core.RightTwoColumn(264),
        new Core.LeftTwoColumn(265),
        new Core.RightTwoColumn(266),
    ],
    scenes: [
    ],
    journals: [
    ],
    finalize: () => {
    }
};

export default apgModule;