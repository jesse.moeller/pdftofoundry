import { PdfData, PdfImage } from "../parse";
import * as Foundry from  '../foundry';
import { IModuleOutput } from "src/IModuleOutput";
import { ModuleData } from "./IAdventure";

export class ModuleScene {
    name: string;
    page: number;
    width: number;
    height: number;
    imageIndex: number;
    imageLeft?: number;
    imageTop?: number;
    imageRight?: number;
    imageBottom?: number;
    imageRows: number;
    imageCols: number;
    cropLeft?: number;
    cropTop?: number;
    cropRight?: number;
    cropBottom?: number;
    rotateCW?: number;
    gridType: 'gridless'|'square'|'hex';
    gridDistance?: number;
    gridUnits?: string;
    globalLight: boolean;
    walls?: object[];
    journals?: { x: number, y: number, name: string }[];
    nameInfo?: { x: number, y: number, page?: number };
    lights?: object[];
    nameFn?: (name: string) => string;

    constructor(init?: Partial<ModuleScene>) {
        Object.assign(this, init);
        if (this.gridType === undefined) {
            this.gridType = 'square';
        }
        if (this.globalLight === undefined) this.globalLight = false;
        if (this.cropLeft === undefined) this.cropLeft = 0;
        if (this.cropTop === undefined) this.cropTop = 0;
        if (this.cropRight === undefined) this.cropRight = this.width;
        if (this.cropBottom === undefined) this.cropBottom = this.height;
        if (this.imageIndex === undefined) this.imageIndex = 0;
    }

    get gridTypeNumber() : number {
        switch (this.gridType) {
            case 'hex': return 4;
            case 'square': return 1;
            case 'gridless': return 0;
        }
        return 0;
    }

    calcFinalSize() {
        let imageWidth = (this.imageRight - this.imageLeft);
        let imageHeight = (this.imageBottom - this.imageTop);
        let cropWidth = this.cropRight - this.cropLeft;
        let cropHeight = this.cropBottom - this.cropTop;
        if (this.rotateCW !== undefined && this.rotateCW % 2 != 0) {
            [imageWidth, imageHeight] = [imageHeight, imageWidth];
            [cropWidth, cropHeight] = [cropHeight, cropWidth];
        }

        if (this.gridType == 'hex') {
            const finalWidth = 100;
            const finalSpacingX = finalWidth * 3 / 4;
            const finalSpacingY = Math.sqrt(3) / 2 * finalWidth;
            let scaleFactorX = (finalSpacingX * this.imageCols) / imageWidth;
            let scaleFactorY = (finalSpacingY * this.imageRows) / imageHeight;
            let offsetX = (this.imageLeft * scaleFactorX) % finalSpacingX;
            // for whatever reason, there's an 18 pixel offset from the top of the grid in hex mode
            let offsetY = (this.imageTop * scaleFactorY - 18 + finalSpacingY) % finalSpacingY;
    
            return [
                Math.round(cropWidth * scaleFactorX),
                Math.round(cropHeight * scaleFactorY),
                Math.round(offsetX),
                Math.round(offsetY)
            ];
        }

        let preSquareWidth = imageWidth / this.imageCols;
        let preSquareHeight = imageHeight / this.imageRows;
        let offsetX = ((this.imageLeft - this.cropLeft) / preSquareWidth * 100) % 100;
        let offsetY = ((this.imageTop - this.cropTop) / preSquareHeight * 100) % 100;

        return [
            Math.round(cropWidth * (100 * this.imageCols) / imageWidth),
            Math.round(cropHeight * (100 * this.imageRows) / imageHeight),
            Math.round(offsetX),
            Math.round(offsetY)
        ];
    }

    generateJournals(moduleData: ModuleData) {
        let ret = [];

        for (const journal of this.journals ?? []) {
            let journalEntry = moduleData.journalEntries.find(j => j.name.startsWith(journal.name));
            if (journalEntry !== undefined) {
                ret.push({
                    "flags": {},
                    "entryId": journalEntry._id,
                    "x": journal.x,
                    "y": journal.y,
                    "icon": "icons/svg/book.svg",
                    "iconSize": 40,
                    "iconTint": "",
                    "text": "",
                    "fontSize": 48,
                    "textAnchor": 1,
                    "textColor": "#FFFFFF"
                });
            }
        }

        return ret;
    }

    async generate(parsedPdf: PdfData, moduleData: ModuleData, prefix: string, output: IModuleOutput) {
        const images = parsedPdf.imageDetails.filter(i => i.page == this.page && i.width == this.width && i.height == this.height);
        const image = this.imageIndex < images.length ? images[this.imageIndex] : undefined;
        if (image === undefined) {
            console.log(`Couldn't find image ${this.name} ${this.page} ${this.width} ${this.height}`);
            return;
        }

        let [resizeWidth, resizeHeight] = this.calcFinalSize();
        let blob = await image.getImage(resizeWidth, resizeHeight, this.cropLeft, this.cropTop, this.cropRight, this.cropBottom, this.rotateCW);
        const finalPath = await output.emitFile(`images`, `${this.name}.png`, blob);

        let [width, height, offsetX, offsetY] = this.calcFinalSize();

        this.updateName(parsedPdf);

        await output.createScene({
            //_id: Foundry.makeid(),
            name: this.name,
            description: '',
            img: finalPath,
            gridType: this.gridTypeNumber,
            grid: 100,
            width: width,
            height: height,
            shiftX: offsetX,
            shiftY: offsetY,
            gridDistance: this.gridDistance,
            gridUnits: this.gridUnits,
            walls: this.walls,
            lights: this.lights,
            notes: this.generateJournals(moduleData),
            globalLight: this.globalLight,
            navigation: false,
        });
    }

    protected updateName(parsedPdf: PdfData) {
        if (this.nameInfo !== undefined) {
            if (this.nameInfo.page == undefined) {
                this.nameInfo.page = this.page;
            }
            let name = parsedPdf.textDetails
                .find(text => text.page == this.nameInfo.page && Math.abs(text.x - this.nameInfo.x) < 1 && Math.abs(text.y - this.nameInfo.y) < 1);
            if (name) {
                this.name = name.text
                    .replace('Pathfinder Flip-Mat:', '')
                    .replace('Pathfinder Flip-Mat Classics:', '')
                    .replace('Pathfinder Flip-Tiles:', '')
                    .trim();

                if (this.nameFn !== undefined) {
                    this.name = this.nameFn(this.name);
                }
            }
        }
    }
}


export class ModuleSceneTiled extends ModuleScene {
    name: string;
    page: number;
    width: number;
    height: number;
    imageLeft?: number;
    imageTop?: number;
    imageRight?: number;
    imageBottom?: number;
    imageRows: number;
    imageCols: number;

    numTileRows: number;
    numTileCols: number;

    tileRotations: number[][];
    tilePositions?: number[][];
    fillColor?: string;

    imageFn?: (image: PdfImage) => { imageLeft: number, imageTop: number, imageRight: number, imageBottom: number };

    constructor(init?: Partial<ModuleSceneTiled>) {
        super(init);
        Object.assign(this, init);
    }

    calcFinalSize() {
        const imageWidth = (this.imageRight - this.imageLeft);
        const imageHeight = (this.imageBottom - this.imageTop);

        return [
            Math.round(this.width * (100 * this.imageCols) / imageWidth),
            Math.round(this.height * (100 * this.imageRows) / imageHeight),
            0,
            0
        ];
    }

    async generate(parsedPdf: PdfData, moduleData: ModuleData, prefix: string, output: IModuleOutput) {
        this.updateName(parsedPdf);

        const canvas = document.createElement("canvas");
        canvas.style.cssText = "border: solid 1px black;";
        //document.body.appendChild(canvas);
        canvas.width = this.imageCols * this.numTileCols * 100;
        canvas.height = this.imageRows * this.numTileRows * 100;
        const ctx = canvas.getContext('2d');

        if (this.fillColor !== undefined) {
            ctx.fillStyle = this.fillColor;
            ctx.fillRect(0, 0, canvas.width, canvas.height);
        }

        let [width, height] = this.calcFinalSize();

        let i = 0;
        for (let image of parsedPdf.imageDetails) {

            let imageLeft, imageRight, imageTop, imageBottom;

            if (this.imageFn !== undefined) {
                const xx = this.imageFn(image);
                if (xx === undefined) continue;

                imageLeft = xx.imageLeft;
                imageRight = xx.imageRight;
                imageTop = xx.imageTop;
                imageBottom = xx.imageBottom;
            } else {
                // the pdfs aren't accurate. some are cropped a little bit, so we do a fuzzy matching
                if (!(image.page == this.page &&
                    Math.abs(image.width - this.width) <= 5 &&
                    Math.abs(image.height - this.height) <= 5)) { continue; }

                imageLeft = this.imageLeft;
                imageRight = this.imageRight;
                imageTop = this.imageTop;
                imageBottom = this.imageBottom;
            }

            let c = Math.floor(i / this.numTileRows);
            let r = Math.floor(i % this.numTileRows);

            if (this.tilePositions !== undefined) {
                [c, r] = this.tilePositions[i];
            }

            let tileCanvas = await image.getImageCanvas();
            ctx.save();
            ctx.translate(
                c * 100 * this.imageCols,
                r * 100 * this.imageRows);
            ctx.translate(100 * this.imageCols / 2, 100 * this.imageRows / 2);
            ctx.rotate(this.tileRotations[r][c] * Math.PI / 2);
            ctx.translate(-100 * this.imageCols / 2, -100 * this.imageRows / 2);
            ctx.drawImage(tileCanvas,
                imageLeft,
                imageTop,
                imageRight - imageLeft,
                imageBottom - imageTop,
                0,
                0,
                100 * this.imageCols,
                100 * this.imageRows);
            ctx.restore();
            i++;
        }

        let blob : Blob = await new Promise(r => canvas.toBlob(b => r(b)));
        const finalPath = await output.emitFile(`images`, `${this.name}.png`, blob);

        await output.createScene({
            name: this.name,
            description: '',
            img: finalPath,
            gridType: 1,
            grid: 100,
            width: canvas.width,
            height: canvas.height,
            shiftX: 0,
            shiftY: 0,
            gridDistance: this.gridDistance,
            gridUnits: this.gridUnits,
            walls: this.walls,
            lights: this.lights,
            notes: this.generateJournals(moduleData),
            globalLight: this.globalLight,
            navigation: false,
        });
    }
}
