import * as Foundry from '../foundry'
import { ModuleDescription, ModuleData } from "./IAdventure";
import { TextDetail, PdfData } from "../parse";
import { IModuleOutput } from "../IModuleOutput";

export class ModuleImporter {
    async import(adventureModule: ModuleDescription, parsedPdf: PdfData, output: IModuleOutput) {
        let moduleData = new ModuleData();

        // check for the watermark
        let watermark = parsedPdf.textDetails.find(x => x.y == 2);
        if (watermark === undefined) {
            output.progressFn("Couldn't detect watermark :(. Please no pirate.", 1);
            return false;
        }

        const watermarkMatch = watermark.text.match(/paizo\.com( #([0-9]+))?, (.*?) <(.*?@.*?)>, (.*)/);
        if (!watermarkMatch) {
            output.progressFn("Couldn't detect watermark :(. Please no pirate.", 1);
            return false;
        }

        // extract the pdf data in the right order first.
        let sections: Record<string, TextDetail[]> = {};
        for (let [i, page] of adventureModule.pages.entries()) {
            output.progressFn(`Extracting page ${i}`, i / adventureModule.pages.length);
            page.getPage(parsedPdf, (section, text) => {
                if (sections[section] === undefined)
                    sections[section] = [];
                sections[section].push(text)
            });
        }

        for (let sectionKey in sections) {
            adventureModule.extractDetails(sections[sectionKey], moduleData);
        }

        adventureModule.finalize(moduleData);

        for (const journal of moduleData.journalEntries) {
            await output.createJournal(journal);
        }

        for (const actor of moduleData.actors) {
            await output.createActor(actor);
        }

        for (const item of moduleData.items) {
            await output.createItem(item);
        }

        for (let [i, journal] of adventureModule.journals.entries()) {
            output.progressFn(`Extracting journal image ${i}`, i / adventureModule.journals.length);
            await journal.import(parsedPdf, output);
        }

        let foundryScenes : Foundry.Scene[] = [];
        for (let [i, scene] of adventureModule.scenes.entries()) {
            output.progressFn(`Extracting scene ${i}`, i / adventureModule.scenes.length);
            await scene.generate(parsedPdf, moduleData, adventureModule.prefix, output);
        }

        output.progressFn("Complete. Enjoy!", 1);

        return true;
    }
}