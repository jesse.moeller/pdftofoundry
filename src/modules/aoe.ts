import { ModuleDescription, ModuleData } from "./IAdventure";
import { ModuleScene } from "./ModuleScene";
import { PdfData, TextDetail } from "../parse";
import { PageLayoutEmitFunction, PageLayout } from "./PageLayouts";
import { Journal } from "../foundry";
import { ParserText, makeContiguousParser } from "./parser";
import { parseMonsterOrHazard } from "./core";

const leftCol1L = 99;
const leftCol2L = 326.25;

export class ChapterHeadingPageLayout extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        const headerBlocks = pdf.textDetails
            .filter(t => t.page == this.pageNum && t.fontName == 'RomicStd-Bold' && t.fontSize > 30)
            .map(t => t.y);
        const headingStartY = Math.min(...headerBlocks) + 10;

        this.getAllTextInBoundingBox(pdf, 0, 1000, 1000, headingStartY, emit);
        this.getAllTextInBoundingBox(pdf, leftCol1L, 300, headingStartY, 40, emit);
        this.getAllTextInBoundingBox(pdf, leftCol2L, 1000, headingStartY, 40, emit);
    }
};

export class LeftSingleColumnWithSidebar extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 250.5, 550, 700, 40, emit);
        this.getAllTextInBoundingBox(pdf, 70, 240, 700, 40, emit, 'sidebar'+this.pageNum);
    }
};

export class RightSingleColumnWithSidebar extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 67.5, 370, 700, 40, emit);
        this.getAllTextInBoundingBox(pdf, 370, 510, 700, 40, emit, 'sidebar'+this.pageNum);
    }
};

export class LeftTwoColumn extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 99, 320, 700, 40, emit);
        this.getAllTextInBoundingBox(pdf, 326.25, 550, 700, 40, emit);
    }
};

export class RightTwoColumn extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 67.5, 290, 700, 40, emit);
        this.getAllTextInBoundingBox(pdf, 294.75, 515, 700, 40, emit);
    }
};

export class LeftTwoColumnWithMap extends PageLayout {
    sectionTop: number;
    
    constructor(pageNum: number, sectionTop?: number) {
        super(pageNum);
        this.sectionTop = sectionTop ?? 327;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 99, 320, this.sectionTop, 40, emit);
        this.getAllTextInBoundingBox(pdf, 326.25, 550, this.sectionTop, 40, emit);
    }
};

export class RightTwoColumnWithMap extends PageLayout {
    sectionTop: number;
    constructor(pageNum: number, sectionTop?: number) {
        super(pageNum);
        this.sectionTop = sectionTop ?? 327;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 67.5, 290, this.sectionTop, 40, emit);
        this.getAllTextInBoundingBox(pdf, 294.75, 515, this.sectionTop, 40, emit);
    }
};

export class LeftTwoColumnWithLeftCutout extends PageLayout {
    cutoutPos: number;
    section?: string;

    constructor(pageNum: number, cutoutPos: number, section?: string) {
        super(pageNum);
        this.cutoutPos = cutoutPos;
        this.section = section;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 108.35, 320, 700, this.cutoutPos, emit, 'sidebar'+this.pageNum);
        this.getAllTextInBoundingBox(pdf, 99, 320, this.cutoutPos, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 326.25, 550, 700, 40, emit, this.section);
    }
};

export class RightTwoColumnWithRightCutout extends PageLayout {
    cutoutPos: number;
    section?: string;

    constructor(pageNum: number, cutoutPos: number, section?: string) {
        super(pageNum);
        this.cutoutPos = cutoutPos;
        this.section = section;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 67.5, 290, 700, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 294.75, 515, this.cutoutPos, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 304.1, 515, 700, this.cutoutPos, emit, 'sidebar'+this.pageNum);
    }
};


export function detectPageLayout(pdf: PdfData, page: number): string {
    const entries = pdf.textDetails.filter(x => x.page == page);

    const leftCol1 = entries.filter(x => Math.abs(x.x - leftCol1L) < 1).length > 5;
    const leftCol2 = entries.filter(x => Math.abs(x.x - leftCol2L) < 1).length > 5;
    const rightCol1 = entries.filter(x => Math.abs(x.x - 67.5) < 1).length > 5;
    const rightCol2 = entries.filter(x => Math.abs(x.x - 294.75) < 1).length > 5;
    const hasHeading = entries.some(x => x.fontName == 'ColiseumBold' && x.fontSize >= 21 && x.y < 720);
    const topOfText = Math.max(...entries.filter(t => t.fontId == 'SabonLTStd-Roman:9' || t.fontId == 'GoodOT:9').map(t => t.y));

    if (rightCol1 && rightCol2) {
        if (hasHeading) {
            //return `new AoE.RightChapterHeading(${page})`;
        } else if (topOfText > 650) {
            return `new AoE.RightTwoColumn(${page})`;
        } else {
            return `new AoE.RightTwoColumnWithMap(${page})`;
        }
    } else if (leftCol1 && leftCol2) {
        if (hasHeading) {
            return `new AoE.ChapterHeadingPageLayout(${page})`;
        } else if (topOfText > 650) {
            return `new AoE.LeftTwoColumn(${page})`;
        } else {
            return `new AoE.LeftTwoColumnWithMap(${page})`;
        }
    } else if (rightCol1) {
        if (hasHeading) {
            return `new AoE.RightChapterHeadingWithSidebar(${page})`;
        } else {
            return `new AoE.RightSingleColumnWithSidebar(${page})`;
        }
    } else if (leftCol1) {
        if (hasHeading) {
            return `new AoE.LeftChapterHeadingWithSidebar(${page})`;
        } else {
            return `new AoE.LeftOneColumnWithSidebar(${page})`;
        }
    } else if (leftCol2 && hasHeading) {
        return `new AoE.LeftTwoColumnWithLeftSidebar(${page})`;
    }

    return '';
}


const parseIcon = (p: ParserText) => {
    return p.consume().text;
};

const parseItalic = makeContiguousParser("<i>", "</i>");
const parseBold = makeContiguousParser("<b>", "</b>");
const parseHeading = makeContiguousParser("<h3>", "</h3>");

const headingFontIds = ['GoodOT-News:12', 'GoodOT:12', 'ColiseumBold:40', 'ColiseumBold:15', 'ColiseumBold:63', 'ColiseumBold:22', 'GoodOT-Bold:12', 'GoodOT-CondBold:12'];
const normalFontIds = ['SabonLTStd-Roman:9', 'Sabon-Roman:9', 'GoodOT:8.5', 'GoodOT:9'];
const subheadingFontIds = [];
const italicFontIds = ['SabonLTStd-Italic:9', 'Sabon-Italic:9', 'TimesNewRomanPS-BoldMT-SC700:6.3', 'TimesNewRomanPS-BoldMT-SC700:9', 'GoodOT-Italic:8.5'];
const boldFontIds = ['TimesNewRomanPS-BoldMT:9', 'TimesNewRomanPSMT:9', 'Sabon-Bold:9', 'Times-Bold:9', 'GoodOT-Bold:8.5'];

const paragraphFontIds = normalFontIds
    .concat(italicFontIds)
    .concat(boldFontIds);

const parseParagraph = (p: ParserText) => {
    let current = '';

    while (headingFontIds.includes(p.peek().fontId)) {
        current += parseHeading(p);
    }

    while (p.anyRemaining()) {
        const peekFont = p.peek().fontId;
        if (normalFontIds.includes(peekFont)) {
            current += p.consume().text;
        } else if (italicFontIds.includes(peekFont)) {
            current += parseItalic(p);
        } else if (boldFontIds.includes(peekFont)) {
            current += parseBold(p);
        } else {
            break;
        }

        if (p.anyRemaining() && p.peek().isIndented) {
            break;
        }
    }
    return "<p>" + current + "</p>";
};

const parseBlockquote = (p: ParserText) => {
    let current = '';
    while (p.anyRemaining()) {
        if (p.peek().fontId == 'GoodOT:9' || p.peek().fontId == 'GoodOT:8.5') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'GoodOT-Italic:9') {
            current += parseItalic(p);
        } else if (p.peek().fontId == 'GoodOT-Bold:9' || p.peek().fontId == 'GoodOT-Bold:8.5') {
            current += parseBold(p);
        } else if (p.peek().fontId == 'Almagro:9') {
            current += p.consume().text;
        } else {
            break;
        }
    }
    return "<blockquote>" + current + "</blockquote>";
};

const parseList = (p: ParserText) => {
    let current = '';

    p.consume();
    
    while (p.anyRemaining()) {
        const peekFont = p.peek().fontId;
        if (normalFontIds.includes(peekFont)) {
            current += p.consume().text;
        } else if (headingFontIds.includes(peekFont)) {
            current += parseHeading(p);
        } else if (italicFontIds.includes(peekFont)) {
            current += parseItalic(p);
        } else if (boldFontIds.includes(peekFont)) {
            current += parseBold(p);
        } else {
            break;
        }

        if (p.anyRemaining() && p.peek().isIndented) {
            break;
        }
    }
    return "<li>" + current + "</li>";
};

const parseAside = (p: ParserText) => {
    return new Journal(p.consume().text, parseBlockquote(p));
};

const parseSection = (p: ParserText, moduleData: ModuleData) => {
    let current = '';

    let name = p.consume().text;
    if (p.peek().fontId == 'GoodOT-Bold:14') {
        name += "(" + p.consume().text + ")";
    }

    while (headingFontIds.includes(p.peek().fontId)) {
        let text = p.consume().text;
        if (['TRIVIAL', 'LOW', 'MODERATE', 'SEVERE', 'EXTREME'].some(x => text.startsWith(x))) {
            name += " (" + text + ")";
        } else {
            name += text;
        }
    }

    while (p.anyRemaining()) {
        let cur = p.peek();
        if (cur.fontId == 'GoodOT:9' && cur.text == '•') {
            current += parseList(p);
        } else if (cur.fontId == 'GoodOT:9' || cur.fontId == 'GoodOT-Bold:9') {
            current += parseBlockquote(p);
        } else if (cur.fontId == 'GoodOT-CondBold:12' && cur.color == '#000000') {
            current += parseMonsterOrHazard(p, moduleData);
        // } else if (cur.fontId == 'GoodOT-CondBold:12' && !(cur.text.match(/^[ABCDEFG]\d+\./) || cur.text.match(/^Event \d+:/))) {
        //     current += parseHeading(p);
        } else if (cur.fontId == 'GoodOT-Bold:11') {
            moduleData.journalEntries.push(parseAside(p));
        } else if (paragraphFontIds.includes(cur.fontId)) {
            current += parseParagraph(p);
        } else {
            //console.log(`abandoning section on: ${cur.fontId} :: (${cur.x}, ${cur.y}) :: ${cur.text}`);
            break;
        }
    }

    moduleData.journalEntries.push(new Journal(name, current));
};

export function extractDetails(text: Array<TextDetail>, moduleData: ModuleData) {
    const blacklistedFonts = ['Times-Italic:8', 'ColiseumBold:14']
    text = text.filter(t => !blacklistedFonts.includes(t.fontId));

    let p = new ParserText(text);

    while (p.anyRemaining()) {
        let cur = p.peek();

        if (cur.fontId == 'GoodOT-Bold:11') {
            moduleData.journalEntries.push(parseAside(p));
        } else if (headingFontIds.includes(cur.fontId) || cur.fontId == 'GoodOT-CondBold:12') {
            //console.log("starting section: " + cur.text);
            parseSection(p, moduleData);
        } else {
            let item = p.consume();
            console.log(`unknown ${item.fontId} :: ${item.page} (${item.x}, ${item.y}) :: ${item.text}`);
        }
    }
}