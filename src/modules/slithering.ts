import { ModuleDescription, ModuleData, AdventureImage, CaptionImage } from "./IAdventure";
import { ModuleScene } from "./ModuleScene";
import { PdfData, TextDetail } from "../parse";
import { PageLayoutEmitFunction, PageLayout } from "./PageLayouts";
import { Journal } from "../foundry";
import { ParserText, makeContiguousParser } from "./parser";
import { parseMonsterOrHazard, sentenceCase } from "./core";

const leftCol1L = 99;
const leftCol2L = 326.25;
const rightCol1L = 67.5;
const rightCol2L = 294.75;

export class ChapterHeadingPageLayout extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        const headerBlocks = pdf.textDetails
            .filter(t => t.page == this.pageNum && t.fontSize > 30)
            .map(t => t.y);
        const headingStartY = Math.min(...headerBlocks) - 10;

        this.getAllTextInBoundingBox(pdf, 0, 1000, 1000, headingStartY, emit);
        this.getAllTextInBoundingBox(pdf, leftCol1L, 300, headingStartY, 40, emit);
        this.getAllTextInBoundingBox(pdf, leftCol2L, 1000, headingStartY, 40, emit);
    }
};

export class RightChapterHeadingPageLayout extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        const headerBlocks = pdf.textDetails
            .filter(t => t.page == this.pageNum && t.fontSize > 30)
            .map(t => t.y);
        const headingStartY = Math.min(...headerBlocks) - 10;

        this.getAllTextInBoundingBox(pdf, 0, 510, 1000, headingStartY, emit);
        this.getAllTextInBoundingBox(pdf, rightCol1L, 290, headingStartY, 40, emit);
        this.getAllTextInBoundingBox(pdf, rightCol2L, 510, headingStartY, 40, emit);
    }
};

export class LeftSingleColumnWithSidebar extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 250.5, 550, 700, 40, emit);
        this.getAllTextInBoundingBox(pdf, 70, 240, 700, 40, emit, 'sidebar'+this.pageNum);
    }
};

export class RightSingleColumnWithSidebar extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, rightCol1L, 370, 700, 40, emit);
        this.getAllTextInBoundingBox(pdf, 370, 510, 700, 40, emit, 'sidebar'+this.pageNum);
    }
};

export class LeftTwoColumn extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 99, 320, 700, 40, emit);
        this.getAllTextInBoundingBox(pdf, 326.25, 550, 700, 40, emit);
    }
};

export class RightTwoColumn extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, rightCol1L, 290, 700, 40, emit);
        this.getAllTextInBoundingBox(pdf, rightCol2L, 515, 700, 40, emit);
    }
};

export class LeftTwoColumnWithMap extends PageLayout {
    sectionTop: number;
    
    constructor(pageNum: number, sectionTop: number) {
        super(pageNum);
        this.sectionTop = sectionTop;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 99, 320, this.sectionTop, 40, emit);
        this.getAllTextInBoundingBox(pdf, 326.25, 550, this.sectionTop, 40, emit);
    }
};

export class RightTwoColumnWithMap extends PageLayout {
    sectionTop: number;
    constructor(pageNum: number, sectionTop: number) {
        super(pageNum);
        this.sectionTop = sectionTop;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, rightCol1L, 290, this.sectionTop, 40, emit);
        this.getAllTextInBoundingBox(pdf, rightCol2L, 515, this.sectionTop, 40, emit);
    }
};

export class RightTwoColumnWithBottomCutout extends PageLayout {
    sectionBottom: number;
    constructor(pageNum: number, sectionBottom: number) {
        super(pageNum);
        this.sectionBottom = sectionBottom;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, rightCol1L, 290, 700, this.sectionBottom, emit);
        this.getAllTextInBoundingBox(pdf, rightCol2L, 515, 700, this.sectionBottom, emit);
    }
};
export class LeftTwoColumnWithLeftCutout extends PageLayout {
    cutoutPos: number;
    section?: string;

    constructor(pageNum: number, cutoutPos: number, section?: string) {
        super(pageNum);
        this.cutoutPos = cutoutPos;
        this.section = section;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 108.35, 320, 700, this.cutoutPos, emit, 'sidebar'+this.pageNum);
        this.getAllTextInBoundingBox(pdf, 99, 320, this.cutoutPos, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 326.25, 550, 700, 40, emit, this.section);
    }
};

export class RightTwoColumnWithRightCutout extends PageLayout {
    cutoutPos: number;
    section?: string;

    constructor(pageNum: number, cutoutPos: number, section?: string) {
        super(pageNum);
        this.cutoutPos = cutoutPos;
        this.section = section;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, rightCol1L, 290, 700, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, rightCol2L, 515, this.cutoutPos, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, rightCol2L+9, 515, 700, this.cutoutPos, emit, 'sidebar'+this.pageNum);
    }
};


export function detectPageLayout(pdf: PdfData, page: number): string {
    const entries = pdf.textDetails.filter(x => x.page == page);

    const topmostHeadingFonts = ['Flyerfonts-Filler'];

    const leftCol1 = entries.filter(x => Math.abs(x.x - leftCol1L) < 1).length > 5;
    const leftCol2 = entries.filter(x => Math.abs(x.x - leftCol2L) < 1).length > 5;
    const rightCol1 = entries.filter(x => Math.abs(x.x - rightCol1L) < 1).length > 5;
    const rightCol2 = entries.filter(x => Math.abs(x.x - rightCol2L) < 1).length > 5;
    const leftSingleCol = entries.filter(x => Math.abs(x.x - 250.5) < 1).length > 5;
    const hasHeading = entries.some(x => topmostHeadingFonts.includes(x.fontName) && x.fontSize >= 21 && x.y < 720);
    const topOfText = Math.max(...entries.filter(t => t.fontId == 'SabonLTStd-Roman:9' || t.fontId == 'GoodOT:9').map(t => t.y));

    if (rightCol1 && rightCol2) {
        if (hasHeading) {
            //return `new RightChapterHeading(${page})`;
        } else if (topOfText > 650) {
            return `new RightTwoColumn(${page})`;
        } else {
            return `new RightTwoColumnWithMap(${page})`;
        }
    } else if (leftCol1 && leftCol2) {
        if (hasHeading) {
            return `new ChapterHeadingPageLayout(${page})`;
        } else if (topOfText > 650) {
            return `new LeftTwoColumn(${page})`;
        } else {
            return `new LeftTwoColumnWithMap(${page})`;
        }
    } else if (rightCol1) {
        if (hasHeading) {
            return `new RightChapterHeadingWithSidebar(${page})`;
        } else {
            return `new RightSingleColumnWithSidebar(${page})`;
        }
    } else if (leftCol1) {
        if (hasHeading) {
            return `new LeftChapterHeadingWithSidebar(${page})`;
        } else {
            return `new LeftSingleColumnWithSidebar(${page})`;
        }
    } else if (leftSingleCol) {
        return `new LeftSingleColumnWithSidebar(${page})`;
    } else if (leftCol2 && hasHeading) {
        return `new LeftTwoColumnWithLeftSidebar(${page})`;
    }

    return '';
}


const parseIcon = (p: ParserText) => {
    return p.consume().text;
};

const parseItalic = makeContiguousParser("<i>", "</i>");
const parseBold = makeContiguousParser("<b>", "</b>");
const parseHeading = makeContiguousParser("<h3>", "</h3>");

const headingFontIds = ['GoodOT-News:12', 'GoodOT:12', 'GoodOT-CondBold:12', 'GoodOT-Bold:12', 'GoodOT-CondBold:14', 'GoodOT-Cond:13', 'GoodOT-Bold:14', 'Flyerfonts-Filler:36', 'Flyerfonts-Filler:14', 'Flyerfonts-Filler:54', 'Flyerfonts-Filler:60'];
const normalFontIds = ['SabonLTStd-Roman:9', 'Sabon-Roman:9', 'GoodOT:8.5', 'GoodOT:9', 'Dax-Regular:9', 'Pathfinder-Icons:9', 'SabonLTStd-Roman:12'];
const italicFontIds = ['SabonLTStd-Italic:9', 'Sabon-Italic:9', 'GoodOT-Italic:8.5', 'GoodOT-Italic:9','SabonLTStd-Italic:12', 'TimesNewRomanPS-BoldItalicMT:9'];
const boldFontIds = ['TimesNewRomanPS-BoldMT:9', 'TimesNewRomanPSMT:9', 'Sabon-Bold:9', 'GoodOT:8.5', 'GoodOT:8', 'GoodOT-Bold:8.5'];

const paragraphFontIds = normalFontIds
    .concat(italicFontIds)
    .concat(boldFontIds);

const parseParagraph = (p: ParserText) => {
    let current = '';


    while (headingFontIds.includes(p.peek().fontId)) {
        current += parseHeading(p);
    }
    
    while (p.anyRemaining()) {
        const peekFont = p.peek().fontId;
        if (normalFontIds.includes(peekFont)) {
            current += p.consume().text;
        } else if (italicFontIds.includes(peekFont)) {
            current += parseItalic(p);
        } else if (boldFontIds.includes(peekFont)) {
            current += parseBold(p);
        } else {
            break;
        }

        if (p.anyRemaining() && p.peek().isIndented) {
            break;
        }
    }
    return "<p>" + current + "</p>";
};

const parseBlockquote = (p: ParserText) => {
    let current = '';
    while (p.anyRemaining()) {
        if (p.peek().fontId == 'GoodOT:9' || p.peek().fontId == 'GoodOT:8.5') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'GoodOT-Italic:9') {
            current += parseItalic(p);
        } else if (p.peek().fontId == 'GoodOT-Bold:9' || p.peek().fontId == 'GoodOT-Bold:8.5') {
            current += parseBold(p);
        } else if (p.peek().fontId == 'Almagro:9') {
            current += p.consume().text;
        } else {
            break;
        }
    }
    return "<blockquote>" + current + "</blockquote>";
};

const parseList = (p: ParserText) => {
    let current = '';

    p.consume();
    
    while (p.anyRemaining()) {
        const peekFont = p.peek().fontId;
        if (normalFontIds.includes(peekFont)) {
            current += p.consume().text;
        } else if (headingFontIds.includes(peekFont)) {
            current += parseHeading(p);
        } else if (italicFontIds.includes(peekFont)) {
            current += parseItalic(p);
        } else if (boldFontIds.includes(peekFont)) {
            current += parseBold(p);
        } else {
            break;
        }

        if (p.anyRemaining() && p.peek().isIndented) {
            break;
        }
    }
    return "<li>" + current + "</li>";
};

const parseAside = (p: ParserText) => {
    return new Journal(p.consume().text, parseBlockquote(p));
};

const parseSection = (p: ParserText, moduleData: ModuleData) => {
    let current = '';

    let name = p.consume().text;
    if (p.peek().fontId == 'GoodOT-Bold:14') {
        name += "(" + p.consume().text + ")";
    }

    while (headingFontIds.includes(p.peek().fontId)) {
        if (p.peek().fontId == 'GoodOT-CondBold:12' && p.peek().color == '#000000') break;

        let text = p.consume().text;
        if (['TRIVIAL', 'LOW', 'MODERATE', 'SEVERE', 'EXTREME'].some(x => text.toUpperCase().startsWith(x))) {
            name += " (" + text + ")";
        } else {
            name += text;
        }
    }

    let nameReorderMatch;
    if (nameReorderMatch = name.match(/^:CHAPTER([0-9]+)(.*)/)) {
        name = `CHAPTER ${nameReorderMatch[1]}: ${nameReorderMatch[2]}`;
    }

    while (p.anyRemaining()) {
        let cur = p.peek();
        if (cur.fontId == 'GoodOT:9' && cur.text == '•') {
            current += parseList(p);
        } else if (cur.fontId == 'GoodOT:9' || cur.fontId == 'GoodOT-Bold:9') {
            current += parseBlockquote(p);
        } else if (cur.fontId == 'GoodOT-CondBold:12' && cur.color == '#000000') {
            current += parseMonsterOrHazard(p, moduleData);
        // } else if (cur.fontId == 'GoodOT-Bold:12' && !(cur.text.match(/^[ABCDEFG]\d+\./) || cur.text.match(/^Event \d+:/))) {
        //     current += parseHeading(p);
        } else if (cur.fontId == 'GoodOT-Bold:11') {
            moduleData.journalEntries.push(parseAside(p));

        } else if (paragraphFontIds.includes(cur.fontId)) {
            current += parseParagraph(p);
        } else {
            //console.log(`abandoning section on: ${cur.fontId} :: (${cur.x}, ${cur.y}) :: ${cur.text}`);
            break;
        }
    }

    moduleData.journalEntries.push(new Journal(name, current));
};

export function extractDetails(text: Array<TextDetail>, moduleData: ModuleData) {
    const blacklistedFonts = ['Times-Italic:8', 'RomicStd-Bold:12', 'TradeGothicLTStd-BdCn20:12', 'TradeGothicLTStd-BdCn20:8', 'Taroca:12', 'TradeGothicLTStd-BdCn20:11.0714', 'CBGBFontSolid:12']
    text = text.filter(t => !blacklistedFonts.includes(t.fontId));

    let p = new ParserText(text);

    while (p.anyRemaining()) {
        let cur = p.peek();

        if (cur.fontId == 'GoodOT-Bold:11') {
            moduleData.journalEntries.push(parseAside(p));
        } else if (headingFontIds.includes(cur.fontId) || cur.fontId == 'GoodOT-CondBold:12') {
            //console.log("starting section: " + cur.text);
            parseSection(p, moduleData);
        } else {
            let item = p.consume();
            console.log(`unknown ${item.fontId} :: ${item.page} (${item.x}, ${item.y}) :: ${item.text}`);
        }
    }
}


const adventureModule : ModuleDescription = {
    prefix: "the-slithering",
    fullName: "The Slithering",
    detect: (pdf: PdfData) => {
        return pdf.textDetails.some(t => t.text == 'THE SLITHERING' && t.fontId == 'Gin-Regular:10');
    },
    extractDetails: extractDetails,
    detectPageLayout: detectPageLayout,
    pages: [
        // chapter 1
        new ChapterHeadingPageLayout(6),
        new RightSingleColumnWithSidebar(7),
        new LeftTwoColumn(8),
        new RightTwoColumn(9),
        new LeftTwoColumnWithLeftCutout(10, 270),
        new RightTwoColumn(11),
        new LeftTwoColumn(12),
        new RightTwoColumn(13),
        new LeftTwoColumn(14),
        new RightTwoColumn(15),
        new LeftTwoColumnWithMap(16, 320),
        new RightTwoColumn(17),
        new LeftTwoColumn(18),
        new RightTwoColumn(19),
        new LeftTwoColumnWithLeftCutout(20, 510),
        new RightTwoColumn(21),
        new LeftTwoColumn(22),
        new RightTwoColumn(23),
        new LeftTwoColumn(24),
        new RightTwoColumn(25),

        // chapter 2
        new ChapterHeadingPageLayout(26),
        new RightSingleColumnWithSidebar(27),
        new LeftTwoColumnWithMap(28, 320),
        new RightTwoColumn(29),
        new LeftTwoColumnWithLeftCutout(30, 430),
        new RightTwoColumn(31),
        new LeftTwoColumnWithLeftCutout(32, 460),
        new RightTwoColumn(33),
        new LeftTwoColumn(34),
        new RightTwoColumn(35),
        new LeftTwoColumn(36),
        new RightTwoColumn(37),
        new LeftTwoColumn(38),
        new RightTwoColumn(39),
        new LeftTwoColumn(40),
        new RightTwoColumn(41),

        // chapter 3
        new ChapterHeadingPageLayout(42),
        new RightSingleColumnWithSidebar(43),
        new LeftTwoColumnWithMap(44, 320),
        new RightTwoColumnWithRightCutout(45, 140),
        new LeftTwoColumn(46),
        new RightTwoColumn(47),
        new LeftTwoColumn(48),
        new RightTwoColumn(49),
        new LeftTwoColumn(50),
        new RightTwoColumn(51),
        new LeftTwoColumn(52),
        new RightTwoColumn(53),
        
        // appendix 1
        new RightChapterHeadingPageLayout(55),
        // map on 56
        new RightTwoColumn(57),
        new LeftTwoColumn(58),
        new RightTwoColumn(59),
        
        // adventure toolbox
        new RightChapterHeadingPageLayout(61),
        new LeftTwoColumn(62),
        new RightTwoColumn(63),
        new LeftSingleColumnWithSidebar(64),
        new RightSingleColumnWithSidebar(65),
    ],
    scenes: [
        new ModuleScene({
            name: 'map1',
            nameInfo: { x: 28.6835, y: 747.6781 },
            nameFn: name => sentenceCase(name),
            page: 2, width: 1551, height: 1246, imageIndex: 0,
            imageLeft: 12, imageRight: 1436, imageCols: 28,
            imageTop: 115, imageBottom: 1132, imageRows: 20,
            globalLight: true,
        }),
        new ModuleScene({
            name: 'map2',
            nameInfo: { x: 643.2435, y: 748.9774 },
            nameFn: name => sentenceCase(name),
            page: 2, width: 1551, height: 1246, imageIndex: 1,
            imageLeft: 13, imageRight: 1538, imageCols: 30,
            imageTop: 64, imageBottom: 1233, imageRows: 23,
            globalLight: true,
        }),
        new ModuleScene({
            name: 'map3',
            nameInfo: { x: 12, y: 730.6681 },
            nameFn: name => sentenceCase(name),
            page: 16, width: 1259, height: 864,
            imageLeft: 15, imageRight: 1240, imageCols: 38,
            imageTop: 32, imageBottom: 837, imageRows: 25,
            globalLight: true,
        }),
        new ModuleScene({
            name: 'map4a',
            nameInfo: { x: 14, y: 733.7548 },
            nameFn: name => sentenceCase(name),
            page: 28, width: 1259, height: 864,
            cropLeft: 0, cropRight: 396, cropTop: 0, cropBottom: 864,
            imageLeft: 20, imageRight: 1235, imageCols: 41,
            imageTop: 9, imageBottom: 838, imageRows: 28,
            globalLight: true,
        }),
        new ModuleScene({
            name: 'map4b',
            nameInfo: { x: 203.625, y: 733.7548 },
            nameFn: name => sentenceCase(name),
            page: 28, width: 1259, height: 864,
            cropLeft: 411, cropRight: 782, cropTop: 0, cropBottom: 864,
            imageLeft: 20, imageRight: 1235, imageCols: 41,
            imageTop: 9, imageBottom: 838, imageRows: 28,
            globalLight: true,
        }),
        new ModuleScene({
            name: 'map4c',
            nameInfo: { x: 393.25, y: 733.7548 },
            nameFn: name => sentenceCase(name),
            page: 28, width: 1259, height: 864,
            cropLeft: 799, cropRight: 1259, cropTop: 0, cropBottom: 864,
            imageLeft: 20, imageRight: 1235, imageCols: 41,
            imageTop: 9, imageBottom: 838, imageRows: 28,
            globalLight: true,
        }),
        new ModuleScene({
            name: 'map5a',
            nameInfo: { x: 13, y: 732.7548 },
            nameFn: name => sentenceCase(name),
            page: 44, width: 1259, height: 865,
            cropLeft: 0, cropRight: 792, cropTop: 0, cropBottom: 437,
            imageLeft: 7, imageRight: 1239, imageCols: 42,
            imageTop: 32, imageBottom: 854, imageRows: 28,
            globalLight: true,
        }),
        new ModuleScene({
            name: 'map5b',
            nameInfo: { x: 13, y: 519 },
            nameFn: name => sentenceCase(name),
            page: 44, width: 1259, height: 865,
            cropLeft: 0, cropRight: 792, cropTop: 454, cropBottom: 865,
            imageLeft: 7, imageRight: 1239, imageCols: 42,
            imageTop: 32, imageBottom: 854, imageRows: 28,
            globalLight: true,
        }),
        new ModuleScene({
            name: 'map5c',
            nameInfo: { x: 396.8623, y: 732.7548 },
            nameFn: name => sentenceCase(name),
            page: 44, width: 1259, height: 865,
            cropLeft: 808, cropRight: 1259, cropTop: 0, cropBottom: 865,
            imageLeft: 7, imageRight: 1239, imageCols: 42,
            imageTop: 32, imageBottom: 854, imageRows: 28,
            globalLight: true,
        }),
        new ModuleScene({
            name: 'map6',
            nameInfo: { x: 46, y: 730.6681 },
            nameFn: name => sentenceCase(name),
            page: 56, width: 1259, height: 1636,
            gridType: 'gridless',
            imageLeft: 0, imageRight: 1000, imageCols: 10,
            imageTop: 0, imageBottom: 1000, imageRows: 10,
            globalLight: true,
        }),
    ],
    journals: [
        new AdventureImage({ name: 'journal1', page: 4, width: 1260, height: 1636 }),
        new AdventureImage({ name: 'journal2', page: 6, width: 1259, height: 805 }),
        new AdventureImage({ name: 'journal3', page: 19, width: 491, height: 513 }),
        new AdventureImage({ name: 'journal4', page: 26, width: 1259, height: 805 }),
        new AdventureImage({ name: 'journal5', page: 42, width: 1259, height: 807 }),
        new AdventureImage({ name: 'journal6', page: 54, width: 1261, height: 1636 }),
        new AdventureImage({ name: 'journal7', page: 55, width: 446, height: 430 }),
        new AdventureImage({ name: 'journal8', page: 60, width: 1260, height: 1636 }),
        new AdventureImage({ name: 'journal9', page: 63, width: 445, height: 629 }),
        new AdventureImage({ name: 'journal10', page: 63, width: 371, height: 497 }),
        new AdventureImage({ name: 'journal11', page: 68, width: 1259, height: 1636 }),
        new AdventureImage({ name: 'journal12', page: 68, width: 1262, height: 715 }),

        new CaptionImage({ name: 'npc1', page: 8, width: 506, height: 541 }),
        new CaptionImage({ name: 'npc2', page: 11, width: 515, height: 551 }),
        new CaptionImage({ name: 'npc3', page: 12, width: 518, height: 518 }),
        new CaptionImage({ name: 'npc4', page: 14, width: 516, height: 552 }),
        new CaptionImage({ name: 'npc5', page: 24, width: 738, height: 1092 }),
        new CaptionImage({ name: 'npc6', page: 29, width: 865, height: 1214 }),
        new CaptionImage({ name: 'npc7', page: 31, width: 846, height: 1007 }),
        new CaptionImage({ name: 'npc8', page: 33, width: 494, height: 528 }),
        new CaptionImage({ name: 'npc9', page: 35, width: 861, height: 1212 }),
        new CaptionImage({ name: 'npc10', page: 41, width: 986, height: 1451 }),
        new CaptionImage({ name: 'npc11', page: 47, width: 884, height: 1248 }),
        new CaptionImage({ name: 'npc12', page: 48, width: 923, height: 1271 }),
        new CaptionImage({ name: 'npc13', page: 51, width: 908, height: 1271 }),
        new CaptionImage({ name: 'npc14', page: 52, width: 927, height: 1062 }),
        new CaptionImage({ name: 'npc15', page: 59, width: 530, height: 1140 }),
        new CaptionImage({ name: 'npc16', page: 62, width: 799, height: 1281 }),
        new CaptionImage({ name: 'npc17', page: 64, width: 866, height: 1033 }),
        new CaptionImage({ name: 'npc18', page: 65, width: 704, height: 996 }),
    ],
    finalize: (moduleData: ModuleData) => {
    }
};

export default adventureModule;